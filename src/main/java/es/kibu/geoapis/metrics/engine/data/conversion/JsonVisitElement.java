package es.kibu.geoapis.metrics.engine.data.conversion;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

/**
 * Created by lrodr_000 on 11/10/2016.
 */
public class JsonVisitElement extends DataSchemaVisitor.VisitElement {

    public JsonVisitElement(Object elem) {
        super(elem);
        if (dataType == null) {
            if (elem instanceof JsonElement) {
                dataType = getType(elem);
            } else {
                throw new RuntimeException("Unknown elem class type: " + elem.getClass().getName());
            }
        }
    }

    private boolean isTyped(JsonObject object){
        return object.has("$type");
    }

    private DataSchemaVisitor.DataType getSimpleType(JsonObject object){
        JsonElement element = object.get("$type");
        String typeName = element.getAsString().toLowerCase();
        switch(typeName) {
            case "integer": return DataSchemaVisitor.DataType.INT;
            case "double" :   return DataSchemaVisitor.DataType.DOUBLE;
            case "string" :   return DataSchemaVisitor.DataType.STRING;
            case "float" :   return DataSchemaVisitor.DataType.FLOAT;
            case "boolean" :   return DataSchemaVisitor.DataType.BOOLEAN;
            case "long" :   return DataSchemaVisitor.DataType.LONG;
        }

        throw new RuntimeException(String.format("Type property value not registered %s", typeName));
    }

    protected DataSchemaVisitor.DataType getType(Object val) {
        JsonElement value = (JsonElement) val;
        if (value.isJsonObject()) {
            if (isTyped((JsonObject) value)) {
                dataType = getSimpleType((JsonObject)value);
            }
            else {
                dataType = DataSchemaVisitor.DataType.RECORD;
            }
        } else if (value.isJsonPrimitive()) {
            JsonPrimitive asJsonPrimitive = value.getAsJsonPrimitive();
            if (asJsonPrimitive.isBoolean()) {
                dataType = DataSchemaVisitor.DataType.BOOLEAN;
            } else if (asJsonPrimitive.isNumber()) {
                dataType = DataSchemaVisitor.DataType.DOUBLE;
            } else if (asJsonPrimitive.isString()) {
                dataType = DataSchemaVisitor.DataType.STRING;
            }
        } else if (value.isJsonNull()) {
            dataType = DataSchemaVisitor.DataType.NULL;
        }

        return dataType;
    }
}
