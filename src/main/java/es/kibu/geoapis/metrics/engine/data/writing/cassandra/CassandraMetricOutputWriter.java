package es.kibu.geoapis.metrics.engine.data.writing.cassandra;

import es.kibu.geoapis.metrics.engine.core.MetricRef;
import es.kibu.geoapis.objectmodel.Metric;

import static es.kibu.geoapis.metrics.engine.data.writing.NamingConventions.getCassandraTableNameFor;
import static es.kibu.geoapis.metrics.engine.data.writing.cassandra.CassandraManager.getCassandraTableNameFor;

/**
 * Created by lrodr_000 on 25/10/2016.
 */
public class CassandraMetricOutputWriter extends GenericCassandraOutputWriter<MetricRef>/*BaseCassandraWriter implements MetricOutputWriter<ScriptObjectMirror> */{

    Metric metric;

    public CassandraMetricOutputWriter(MetricRef metricRef, Metric metric) {
        super(metricRef);
        this.metric = metric;
        assert this.metric != null;
        createTableIfNotExist();
    }

    protected String getOutputSchema() {
        return metric.getOutput().getSchema();
    }

    protected String getCassandraTableName() {
        return CassandraManager.getCassandraTableNameFor(this.reference);
    }

    @Override
    public CassandraConfigurator getConfigurator() {
        return null;
    }
}
