package es.kibu.geoapis.metrics.engine.core;

import es.kibu.geoapis.objectmodel.Action;

import java.io.Serializable;

/**
 * Created by lrodriguez2002cu on 18/01/2017.
 * Defines the required information for identifying an action.
 * It can be seen as the coordinates to properly identify an action.
 */
public class ActionRef implements Serializable {

    String application;
    String user;
    String session;
    String action;
    String metricDefinitionId;
    String actionType;

    public String getActionTarget() {
        return actionTarget;
    }

    public void setActionTarget(String actionTarget) {
        this.actionTarget = actionTarget;
    }

    String actionTarget;

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public String getApplication() {
        return application;
    }

    public void setApplication(String application) {
        this.application = application;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getMetricDefinitionId() {
        return metricDefinitionId;
    }

    public void setMetricDefinitionId(String metricDefinitionId) {
        this.metricDefinitionId = metricDefinitionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ActionRef actionRef = (ActionRef) o;

        if (!getApplication().equals(actionRef.getApplication())) return false;
        if (getUser() != null ? !getUser().equals(actionRef.getUser()) : actionRef.getUser() != null) return false;
        if (getSession() != null ? !getSession().equals(actionRef.getSession()) : actionRef.getSession() != null)
            return false;
        if (!getAction().equals(actionRef.getAction())) return false;
        if (!getMetricDefinitionId().equals(actionRef.getMetricDefinitionId())) return false;
        if (!getActionType().equals(actionRef.getActionType())) return false;
        return getActionTarget().equals(actionRef.getActionTarget());
    }

    @Override
    public int hashCode() {
        int result = getApplication().hashCode();
        result = 31 * result + (getUser() != null ? getUser().hashCode() : 0);
        result = 31 * result + (getSession() != null ? getSession().hashCode() : 0);
        result = 31 * result + getAction().hashCode();
        result = 31 * result + getMetricDefinitionId().hashCode();
        result = 31 * result + getActionType().hashCode();
        result = 31 * result + getActionTarget().hashCode();
        return result;
    }

    public ActionRef(String application, String user, String session, String action, String metricDefinitionId, String actionType, String actionTarget) {
        this.application = application;
        this.user = user;
        this.session = session;
        this.action = action;
        this.metricDefinitionId = metricDefinitionId;
        this.actionType = actionType;
        this.actionTarget = actionTarget;

        assert action != null;
        assert metricDefinitionId != null;
        assert application != null;

        assert this.actionType != null;
        assert this.actionTarget != null;

        assert ( this.actionTarget.equalsIgnoreCase(Action.TARGET_APPLICATION)
                || this.actionTarget.equalsIgnoreCase(Action.TARGET_USER) || this.actionTarget.equalsIgnoreCase(Action.TARGET_SESSION));

        assert ( this.actionType.equalsIgnoreCase(Action.PUSH_NOTIFICATION)
                || this.actionType.equalsIgnoreCase(Action.URL_POST));

    }

    public ActionRef() {
    }

    @Override
    public String toString() {
        return "ActionRef{" +
                "application='" + application + '\'' +
                ", user='" + user + '\'' +
                ", session='" + session + '\'' +
                ", action='" + action + '\'' +
                ", metricDefinitionId='" + metricDefinitionId + '\'' +
                ", actionType='" + actionType + '\'' +
                ", actionTarget='" + actionTarget + '\'' +
                '}';
    }
}
