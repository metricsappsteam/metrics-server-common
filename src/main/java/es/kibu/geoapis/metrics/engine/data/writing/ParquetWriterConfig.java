package es.kibu.geoapis.metrics.engine.data.writing;

import es.kibu.geoapis.metrics.engine.data.writing.core.WriterConfig;
//import es.kibu.geoapis.metrics.utils.DataTools;

/**
 * Created by lrodr_000 on 24/02/2017.
 */
public class ParquetWriterConfig extends WriterConfig {

    public ParquetWriterConfig(String parquetFileName) {
        setParquetFileName(parquetFileName);
    }

    void setParquetFileName(String parquetFileName) {
        //setConfigValue(DataTools.ParquetDataWriter.PARQUET_FILE_NAME, parquetFileName);
    }

    String getParquetFilename() {
        //return (String) getConfigValue(DataTools.ParquetDataWriter.PARQUET_FILE_NAME);
        throw new RuntimeException("Not implemented after refactoring");
    }
}
