package es.kibu.geoapis.metrics.engine.data.writing.core;

import es.kibu.geoapis.objectmodel.MetricsDefinition;

/**
 * Created by lrodr_000 on 23/02/2017.
 */
public interface DataWriter {

    void setConfig(WriterConfig config);

    WriterConfig getConfig();

    void write(String jsonFile, MetricsDefinition metricsDefinition) throws Exception;
}
