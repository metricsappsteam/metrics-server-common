package es.kibu.geoapis.metrics.engine.evaluation;

import es.kibu.geoapis.objectmodel.Metric;

/**
 * For now this strategy is in charge of providing the metrics to be evaluated in the order defined in the strategy
 */

public interface EvaluationStrategy {
     Metric getNextMetric();
     void reset();
}
