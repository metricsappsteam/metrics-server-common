package es.kibu.geoapis.metrics.engine.data.conversion;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.fge.jackson.JsonLoader;
import es.kibu.geoapis.converters.FormatsSupported;
import es.kibu.geoapis.metrics.utils.Utils;
import es.kibu.geoapis.objectmodel.CustomDimension;
import es.kibu.geoapis.objectmodel.Dimension;
import es.kibu.geoapis.objectmodel.ProvidedDimension;
import es.kibu.geoapis.objectmodel.Variable;

import java.io.IOException;

/**
 * Created by lrodriguez2002cu on 31/10/2016.
 */
public class VariableConverter {

    public static JsonNode getVariableSchema(Variable variable) {
        try {
            ObjectMapper mapper = new ObjectMapper();

            ObjectNode resultNode = (ObjectNode) mapper.readTree(objectSchemaBase);
            ObjectNode properties = JsonNodeFactory.instance.objectNode();
            for (Dimension dimension : variable.getDimensions()) {
                JsonNode dimensionSchema = getDimensionSchema(dimension);
                properties.replace(dimension.getName(), dimensionSchema);
            }

            resultNode.replace("properties", properties);

            return resultNode;
        } catch (IOException e) {
            throw new RuntimeException("Error while parsing the variable" + variable.getName());
        }
    }

    private static ObjectNode typeNode(String typeName) {
        ObjectNode object = JsonNodeFactory.instance.objectNode();
        object.put("type", typeName);
        return object;
    }


    public static final String LocationSchema = "{\n" +
            "   \"type\" : \"object\",\n" +
            "   \"properties\": {\n" +
            "       \"latitude\": { \"type\": \"number\"},\n" +
            "       \"longitude\": { \"type\": \"number\"},\n" +
            "       \"altitude\": { \"type\": \"number\"},\n" +
            "       \"accuracy\": { \"type\": \"number\"},\n" +
            "       \"time\": { \"type\": \"string\"}\n" +
            "   }\t\n" +
            "}\n";

    public static final String OrientationSchema = "{\n" +
            "   \"type\" : \"object\",\n" +
            "   \"properties\": {\n" +
            "       \"x\": { \"type\": \"number\"},\n" +
            "       \"y\": { \"type\": \"number\"},\n" +
            "       \"z\": { \"type\": \"number\"}\n" +
            "   }\t\n" +
            "}\n";
/*

    public static final String SessionSchema = "{\n" +
            "   \"type\" : \"object\",\n" +
            "   \"properties\": {\n" +
            "       \"id\": { \"type\": \"string\"},\n" +
            "       \"startTime\": { \"type\": \"string\"}\n" +
            "      \n" +
            "   }\t\n" +
            "}\n";

*/

    public static final String StringSchemaType =
            "{\n" +
                    "   \"type\" : \"string\"\n" +
                    "}\n";

    public static final String StringTimeGUUIDSchemaType = "{\n" +
            "   \"type\" : \"string\",\n" +
            "\"format\" : " + Utils.quote(FormatsSupported.TIMEUUID.toFormatString()) +
            "}\n";

    public static final String StringTimeStampSchemaType =
            "{\n" +
                    "   \"type\" : \"string\",\n" +
                    "   \"format\" : " + Utils.quote(FormatsSupported.DATE_TIME.toFormatString()) +
                    "}\n";

    public static final String objectSchemaBase =
            "{\n" +
                    "   \"type\" : \"object\",\n" +
                    "   \"properties\": {\n" +
                    "      \n" +
                    "   }\t\n" +
                    "}\n";


    public static enum Types {
        INTEGER,
        STRING, DOUBLE, BOOLEAN;

        public static Types fromString(String strType) {
            if (strType.equalsIgnoreCase(INTEGER_STR)) {
                return INTEGER;
            } else if (strType.equalsIgnoreCase(STRING_STR)) {
                return STRING;
            } else if (strType.equalsIgnoreCase(DOUBLE_STR)) {
                return DOUBLE;
            } else if (strType.equalsIgnoreCase(BOOLEAN_STR)) {
                return BOOLEAN;
            }
            throw new RuntimeException("Unknown type for type string provided " + strType);
        }

    }

    public static final String INTEGER_STR = "integer";
    public static final String STRING_STR = "string";
    public static final String DOUBLE_STR = "double";
    public static final String BOOLEAN_STR = "boolean";


    /**
     * This function handles the conversion for the default types
     */
    private static JsonNode getDimensionSchema(Dimension dimension) throws IOException {

        ObjectMapper mapper = new ObjectMapper();

        if (dimension instanceof ProvidedDimension) {
            ProvidedDimension providedDimension = (ProvidedDimension) dimension;

            if (providedDimension.isLocation()) {
                return mapper.readTree(LocationSchema);
            }

            if (providedDimension.isUser()
                    || providedDimension.isSession()
                    || providedDimension.isApplication()
                    ) {
                return mapper.readTree(StringSchemaType);
            }

            if (providedDimension.isTime()) {
                return mapper.readTree(StringTimeStampSchemaType);
            }

            if (providedDimension.isTimeId() || providedDimension.isPrevId()) {
                return mapper.readTree(StringTimeGUUIDSchemaType);
            }

            /*if (providedDimension.isSession()) {
                return mapper.readTree(SessionSchema);
            }*/
            if (providedDimension.isOrientation()) {
                return mapper.readTree(OrientationSchema);
            }
            if (providedDimension.isProviderClass()) {
                return mapper.readTree(providedDimension.getProdiverOutputSchema());
            }
        } else {
            if (dimension instanceof CustomDimension) {
                CustomDimension customDimension = (CustomDimension) dimension;
                if (customDimension.isSimple()) {
                    Types type = Types.fromString(dimension.getType());
                    switch (type) {
                        case BOOLEAN:
                            return typeNode("boolean");
                        case DOUBLE:
                            return typeNode("number");
                        case STRING:
                            return typeNode("string");
                        case INTEGER:
                            return typeNode("integer");
                    }
                } else {
                    return JsonLoader.fromString(customDimension.getDimensionSchema());
                }
            }
        }

        throw new RuntimeException("The supplied dimension type is not registered" + dimension.toString());
    }


}
