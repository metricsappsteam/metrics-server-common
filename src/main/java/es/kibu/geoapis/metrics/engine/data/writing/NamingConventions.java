package es.kibu.geoapis.metrics.engine.data.writing;

import es.kibu.geoapis.metrics.engine.core.ActionRef;
import es.kibu.geoapis.metrics.engine.core.MetricRef;
import es.kibu.geoapis.objectmodel.MetricsDefinition;
import es.kibu.geoapis.objectmodel.Variable;

import java.util.UUID;

/**
 * Created by lrodriguez2002cu on 06/11/2016.
 */
public class NamingConventions {


    static String variableTableName;
    static boolean overrideVariableTableName;


    static String metricTableName;
    static boolean overrideMetricTableName;


    private static void overrideTableName(String tableName, boolean variable) {
        if (variable) {
            variableTableName = tableName;
            overrideVariableTableName = true;
        } else {
            metricTableName = tableName;
            overrideMetricTableName = true;
        }
    }

    private static void stopOverrideTableName(boolean variable) {
        if (variable) {
            variableTableName = "";

            overrideVariableTableName = false;
        } else {
            metricTableName = "";
            overrideMetricTableName = false;
        }
    }


    public static void overrideVariableTableName(String tableName) {
        overrideTableName(tableName, true);
    }

    public static void stopOverrideVariableTableName() {
        stopOverrideTableName(true);
    }

    public static void overrideMetricTableName(String tableName) {
        overrideTableName(tableName, false);
    }

    public static void stopOverrideMetricTableName() {
        stopOverrideTableName(false);
    }

    private static String getAppBasicPart(String application) {
        return application.replace("application-", "app-").replace("-", "_");
    }

    public static String getCassandraTableNameFor(MetricRef metricRef) {
        assert metricRef != null;
        assert metricRef.getMetric() != null || metricRef.getMetricName() != null;

        String app = getAppBasicPart(metricRef.getApplication());
        return String.format("%s_%s", app, (metricRef.getMetric() != null)? metricRef.getMetric(): metricRef.getMetricName());
    }

    public static String getCassandraTableNameFor(ActionRef actionRef) {
        String app = getAppBasicPart(actionRef.getApplication());
        return String.format("%s_%s", app, actionRef.getAction());
    }

    public static String getVariableTableName(Variable variable, MetricsDefinition definition) {
        //'variable${variable.getName()}${metric.getId()}
        String variableName = variable.getName();
        String definitionId = definition.getId();
        return getVariableTableName(variableName, definitionId);
    }

    public static String getVariableTableName(String variableName, String definitionId) {
        return (overrideVariableTableName) ? variableTableName : String.format("var_%s%s", variableName, definitionId);
    }

    public static class ApplicationIdCreator {
        public static String createId() {
            UUID uuid = UUID.randomUUID();
            return String.format("app-%s", /*Long.toString(Math.abs(uuid.getLeastSignificantBits())),*/
                    Long.toString(Math.abs(uuid.getMostSignificantBits()), 16));
        }
    }


}
