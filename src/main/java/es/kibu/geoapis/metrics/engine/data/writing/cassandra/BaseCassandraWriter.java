package es.kibu.geoapis.metrics.engine.data.writing.cassandra;

import com.fasterxml.jackson.databind.JsonNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by lrodriguez2002cu on 31/10/2016.
 */
public class BaseCassandraWriter {

    Logger logger = LoggerFactory.getLogger(BaseCassandraWriter.class);

    boolean tableCreated = false;
    String tableName;


    public boolean isTableCreated(String tableName) {
        return tableCreated && this.tableName.equalsIgnoreCase(tableName);
    }

    public void setTableCreated(boolean tableCreated, String tableName) {
        this.tableName = tableName;
        this.tableCreated = tableCreated;
    }

    public CassandraManager getCassandraManager() {
        return CassandraManager.getInstance(getConfigurator());
    }

    public BaseCassandraWriter() {
        //this.configurator = new DSCassandraManagerConfigurator();
    }

    CassandraConfigurator configurator;

    public void setConfigurator(CassandraConfigurator configurator) {
        this.configurator = configurator;
    }

    public CassandraConfigurator getConfigurator(){
        return this.configurator;
    }

    public void createTableIfNotExists(String cassandraTableName, JsonNode schemaWrappedJsonNode, CassandraManager.CassandraManagerConfig config) {
        if (!isTableCreated(cassandraTableName)) {
            logger.debug("the table is not created so creating...");

            getCassandraManager().createCassandraTable(cassandraTableName, schemaWrappedJsonNode, config);
            logger.debug("the table created");
            setTableCreated(true, cassandraTableName);
        }
    }

    public String getKeyspace() {
        return getCassandraManager().getKeyspace();
    }


}
