package es.kibu.geoapis.metrics.engine.evaluation;

import es.kibu.geoapis.objectmodel.Metric;
import es.kibu.geoapis.objectmodel.MetricsDefinition;

/**
 * Created by lrodriguez2002cu on 17/10/2016.
 */
public class SingleMetricStrategy implements EvaluationStrategy {

    MetricsDefinition definition;
    String metric;
    boolean returned = false;

    public SingleMetricStrategy(MetricsDefinition definition, String metricName) {
        this.definition = definition;
        this.metric = metricName;
        if (definition.getMetricByName(metricName)== null) {
            throw new RuntimeException(String.format("The metric must exist in the metric %s definition to use this strategy", metricName));
        }
    }

    @Override
    public Metric getNextMetric() {
        if (!returned) {
            returned = true;
            return definition.getMetricByName(metric);
        }
        return null;
    }

    @Override
    public void reset() {
        returned = false;
    }
}
