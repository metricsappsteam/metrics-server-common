package es.kibu.geoapis.metrics.engine.data.writing;

/**
 * Created by lrodr_000 on 19/01/2017.
 */
public interface WriteNotifier {

    void notifyWrite();

}
