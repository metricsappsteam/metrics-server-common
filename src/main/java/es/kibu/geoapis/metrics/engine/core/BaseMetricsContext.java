package es.kibu.geoapis.metrics.engine.core;

import java.io.Serializable;

/**
 * Created by lrodriguez2002cu on 12/03/2017.
 */

public interface BaseMetricsContext extends Serializable {

    String getApplicationId();

    String getUserId();

    String getSessionId();

}
