package es.kibu.geoapis.metrics.engine.data.conversion;

import es.kibu.geoapis.metrics.engine.data.conversion.DataSchemaVisitor;
import es.kibu.geoapis.metrics.engine.data.conversion.MapVisitor;
import jdk.nashorn.api.scripting.ScriptObjectMirror;

import java.util.Iterator;
import java.util.Map;

import static es.kibu.geoapis.metrics.engine.data.conversion.DataSchemaVisitor.DataType.RECORD;

/**
 * Created by lrodriguez2002cu on 11/10/2016.
 */
public class ObjectMirrorVisitor extends MapVisitor {


    private final ScriptObjectMirror scriptObjectMirror;

    public static class ObjectMirrorVisitElement extends VisitElement {

        public ObjectMirrorVisitElement(Object elem) {
            super(elem);
            if (dataType == null){ //if not solved by the Map
                if (elem instanceof ScriptObjectMirror) {
                    getType(elem);
                }
                else { throw new RuntimeException("Unknown elem class type: " + elem.getClass().getName());}
            }

        }

        protected DataSchemaVisitor.DataType getType(Object val) {
            if (val instanceof ScriptObjectMirror) {
                dataType = RECORD;
            } else {
                if (val instanceof Map.Entry) {
                    Map.Entry value = (Map.Entry) val;
                    Object value1 = value.getValue();
                    getTypeFor(value1);
                } else {
                    getTypeFor(val);
                    //dataType = DataSchemaVisitor.DataType.NULL;
                }
            }
            return dataType;
        }

        private void getTypeFor(Object value) {
            if (value == null) {
                dataType = DataType.NULL;
            } else if (value instanceof Boolean) {
                dataType = DataType.BOOLEAN;
            } else if (value instanceof Double) {
                dataType = DataType.DOUBLE;
            } else if (value instanceof String) {
                dataType = DataType.STRING;
            } else if (value instanceof Integer) {
                dataType = DataType.INT;
            } else if (value instanceof Float) {
                dataType = DataType.FLOAT;
            } else if (value instanceof Long) {
                dataType = DataType.LONG;
            } else if (value instanceof ScriptObjectMirror) {
                dataType = DataType.RECORD;
            }
            this.value = value;

            if (dataType == null) throw new RuntimeException("Unknown type to convert: " + value.getClass().getName());
        }
    }


/*

    List<Row> rows = new ArrayList<>();
        if (object instanceof ScriptObjectMirror) {
        for (Map.Entry<String, Object> entry : ((ScriptObjectMirror) object).entrySet()) {
            Object value = entry.getValue();

            Object unwrapped = ((WrappedClass)value).getJavaObject();
            if (unwrapped instanceof Row) {
                rows.add((Row)unwrapped);
            }
        }
    }
        return rows;
*/

    public ObjectMirrorVisitor(ScriptObjectMirror objectMirror) {
        scriptObjectMirror = objectMirror;
    }

    @Override
    VisitElement newVisitElement(Object o) {
        return new ObjectMirrorVisitElement(o);
    }

    @Override
    protected Object getRoot() {
        return scriptObjectMirror;
    }

    @Override
    protected Iterator<Map.Entry<String, Object>> getIteratorFor(Object next) {
        Iterator<Map.Entry<String, Object>> iteratorFor = super.getIteratorFor(next);
        if (iteratorFor!= null) return iteratorFor;

        if (next instanceof ScriptObjectMirror) {
            return  (Iterator<Map.Entry<String, Object>>)(Object)((ScriptObjectMirror) next).entrySet().iterator();
        }
        else throw new RuntimeException("No type entry for getIteratorFor: "+ next.getClass().getName());
    }
}
