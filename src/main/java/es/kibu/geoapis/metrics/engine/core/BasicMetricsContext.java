package es.kibu.geoapis.metrics.engine.core;

/**
 * Created by lrodriguez on 13/03/2017.
 */

/*
  A basic implementation of a metrics context
* */
public class BasicMetricsContext implements BaseMetricsContext {

    String applicationId;
    String userId;
    String sessionId;

    public BasicMetricsContext(String applicationId, String userId, String sessionId) {
        this.applicationId = applicationId;
        this.userId = userId;
        this.sessionId = sessionId;
    }

    public BasicMetricsContext() {
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    @Override
    public String getApplicationId() {
        return applicationId;
    }

    @Override
    public String getUserId() {
        return userId;
    }

    @Override
    public String getSessionId() {
        return sessionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BasicMetricsContext that = (BasicMetricsContext) o;

        if (getApplicationId() != null ? !getApplicationId().equals(that.getApplicationId()) : that.getApplicationId() != null)
            return false;
        if (getUserId() != null ? !getUserId().equals(that.getUserId()) : that.getUserId() != null) return false;
        return getSessionId() != null ? getSessionId().equals(that.getSessionId()) : that.getSessionId() == null;
    }

    @Override
    public int hashCode() {
        int result = getApplicationId() != null ? getApplicationId().hashCode() : 0;
        result = 31 * result + (getUserId() != null ? getUserId().hashCode() : 0);
        result = 31 * result + (getSessionId() != null ? getSessionId().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "BasicMetricsContext{" +
                "applicationId='" + applicationId + '\'' +
                ", userId='" + userId + '\'' +
                ", sessionId='" + sessionId + '\'' +
                '}';
    }
}
