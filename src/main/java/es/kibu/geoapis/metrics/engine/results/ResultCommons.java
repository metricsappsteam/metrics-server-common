package es.kibu.geoapis.metrics.engine.results;

/**
 * Created by lrodriguez2002cu on 21/10/2016.
 */
public class ResultCommons {

    public static final String METRIC_TIME = "time";

    public static final String VALUE = "value";

    public static final String CASSANDRA_DEFAULT_KEYSPACE = "metrics";

}
