package es.kibu.geoapis.metrics.engine.results;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by lrodriguez2002cu on 18/10/2016.
 */
public class MetricEvaluationResults implements Serializable {

    Map<String, Object> results = new HashMap();

    public Map<String, Object> getResults() {
        return results;
    }

    public void add(String metricName, MetricEvaluationResult result){
        results.put(metricName, result);
    }


    public MetricEvaluationResult getResultForMetric(String metric){
        for (Object o : results.entrySet()) {
            Map.Entry entry = (Map.Entry) o;
            if (entry.getValue() instanceof MetricEvaluationResult) {
                if (entry.getKey().toString().equalsIgnoreCase(metric)) {
                    MetricEvaluationResult res = ((MetricEvaluationResult) entry.getValue());
                    //logger.debug(String.format("%s -> %s", entry.getKey(), res.toString()));
                    return res;
                }
            }
        }
        return null;
    }

    public <T> T getResultForMetricAs(String metric) {
        MetricEvaluationResult metricEvaluationResult = getResultForMetric(metric);
        if (metricEvaluationResult == null) {
            return null;
        } else
            return metricEvaluationResult.getAs();
    }

}
