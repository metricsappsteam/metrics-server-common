package es.kibu.geoapis.metrics.engine.data.conversion;

import com.google.gson.JsonObject;

import java.util.Iterator;
import java.util.Map;
import java.util.Stack;

/**
 * Created by lrodriguez2002cu on 11/10/2016.
 */
public abstract class MapVisitor implements DataSchemaVisitor<DataSchemaVisitor.VisitElement>, Iterable<DataSchemaVisitor.VisitElement> {

    boolean firstTime = false;
    VisitElement currentElem;
    Stack<Iterator<Map.Entry<String, Object>>> iteratorsStack = new Stack<>();
    boolean ended = false;

    public MapVisitor() {
        firstTime = true;
    }

    /*@Override
        public ItemType getCurrentType() {
            if (currentElem != null) {
                return currentElem.getItemType();
            }
            return null;
        }
        */
    @Override
    public String getCurrentFieldName() {
        if (currentElem != null) {
            return currentElem.getName();
        }
        return null;
    }

    @Override
    public DataType getCurrentDataType() {
        if (currentElem != null) {
            return currentElem.getDataType();
        }
        return null;
    }

    @Override
    public Object getCurrentValue() {
        if (currentElem != null) {
            return currentElem.getValue();
        }
        return null;
    }

    @Override
    public Iterator<VisitElement> iterator() {
        return this;
    }

    @Override
    public VisitElement next() {
        currentElem = nextElem();
        return currentElem;
    }

    abstract VisitElement newVisitElement(Object o);

    public VisitElement nextElem() {
        if (hasNext()) {
            if (firstTime) {
                VisitElement visitElement = newVisitElement(getRoot());
                if (visitElement.getDataType() == DataType.RECORD)
                    iteratorsStack.push(getIteratorFor(getRoot())/*((JsonObject) root).entrySet().iterator()*/);
                firstTime = false;
                return visitElement;
            } else {
                Iterator<Map.Entry<String, Object>> peek = null;
                Iterator iterator = null;
                if (!iteratorsStack.empty()) {
                    peek = iteratorsStack.peek();
                    iterator = getIterator();
                }

                if (iterator != peek || iteratorsStack.empty()) {// record finished
                    if (iteratorsStack.empty()) ended = true;
                    return VisitElement.END_RECORD();
                } else if (iterator != null) {
                    Map.Entry next = (Map.Entry) iterator.next();
                    VisitElement visitElement = newVisitElement(next);
                    if (visitElement.getDataType() == DataType.RECORD) {
                        iteratorsStack.push(getIteratorFor(next));
                    }
                    return visitElement;
                } else return null;
            }

        } else return null;
    }

    protected abstract Object getRoot();

    protected Iterator<Map.Entry<String, Object>> getIteratorFor(Object next) {
        if (next instanceof JsonObject) {
            return  (Iterator<Map.Entry<String, Object>>)(Object)((JsonObject) next).entrySet().iterator();
        }
        else if (next  instanceof Map.Entry){
            Map.Entry entry = (Map.Entry)next;
            if (entry.getValue() instanceof Map){
                return (Iterator<Map.Entry<String, Object>>)((Map) entry.getValue()).entrySet().iterator();
            }
            else if (entry.getValue() instanceof JsonObject) {
                return (Iterator<Map.Entry<String, Object>>)(Object)((JsonObject) entry.getValue()).entrySet().iterator();
            }
            //throw new RuntimeException("No type entry for getIteratorFor: "+ next.getClass().getName());
        }
        return null;//throw new RuntimeException("No type entry for getIteratorFor: "+ next.getClass().getName());
    }

    @Override
    public boolean hasNext() {
        return hasNextInStack();
    }

    private boolean hasNextInStack() {
        if (iteratorsStack.empty() || firstTime) {
            return !ended || firstTime;
        } else {
            boolean has = iteratorsStack.peek().hasNext();
            if (!has) {
                Iterator it = iteratorsStack.pop();
                boolean hasUnested = hasNextInStack();
                iteratorsStack.push(it);
                return hasUnested;
            } else return has;
        }
    }

    private Iterator getIterator() {
        if (iteratorsStack.empty()) return null;
        else {
            boolean has = iteratorsStack.peek().hasNext();
            if (!has) {
                iteratorsStack.pop();
                return null;//getIterator();
            } else return iteratorsStack.peek();
        }
    }
}
