package es.kibu.geoapis.metrics.engine.data.writing;

import es.kibu.geoapis.objectmodel.MetricsDefinition;

/**
 * Created by lrodr_000 on 24/02/2017.
 */
public class WritingUtils {
    public static String getVariableName(String variableName, MetricsDefinition metricsDefinition) {
        //variable${variable.getName()}${metric.getId()}
        return NamingConventions.getVariableTableName(variableName, metricsDefinition.getId());
        //String.format("variable%s%s", variableName, metricsDefinition.getId());
    }
}
