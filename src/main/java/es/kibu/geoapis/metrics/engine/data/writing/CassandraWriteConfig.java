package es.kibu.geoapis.metrics.engine.data.writing;

import es.kibu.geoapis.metrics.engine.data.writing.core.WriterConfig;
import es.kibu.geoapis.objectmodel.MetricsDefinition;
import es.kibu.geoapis.objectmodel.Variable;

/**
 * Created by lrodr_000 on 24/02/2017.
 */
public class CassandraWriteConfig extends WriterConfig {

    public CassandraWriteConfig(String keyspace, String tableName, Variable variable, MetricsDefinition metricsDefinition) {
        setKeyspace(keyspace);
        setTableName(tableName);
        setVariable(variable);
        setMetricDefinition(metricsDefinition);
    }

    public void setTableName(String tableName) {
        setConfigValue(CassandraDataWriter.TABLE, tableName);
    }

    public void setKeyspace(String keyspace) {
        setConfigValue(CassandraDataWriter.KEYSPACE, keyspace);
    }

    public void setMetricDefinition(MetricsDefinition metricDefinition) {
        setConfigValue(CassandraDataWriter.METRIC_DEFINITION, metricDefinition);
    }

    public void setVariable(Variable variable) {
        setConfigValue(CassandraDataWriter.VARIABLE, variable);
    }

    MetricsDefinition getMetricDefinition() {
        Object configValue = getConfigValue(CassandraDataWriter.METRIC_DEFINITION);
        return ((MetricsDefinition) configValue);
    }

    public String getTableName() {
        return (String) getConfigValue(CassandraDataWriter.TABLE);
    }

    public String getKeyspace() {
        return (String) getConfigValue(CassandraDataWriter.KEYSPACE);
    }

    public Variable getVariable() {
        Object configValue = getConfigValue(CassandraDataWriter.VARIABLE);
        return ((Variable) configValue);
    }

}
