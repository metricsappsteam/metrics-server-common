package es.kibu.geoapis.metrics.engine.core;

/**
 * Created by lrodr_000 on 13/03/2017.
 */
public class Constants {
    public static final String SCRIPT_ENGINE_FRONTEND_ACTOR_NAME = "scriptEngineFrontend";

    public static final String METRICS_ENGINE_BACKEND_SYSTEM = "MetricsEngineBackendSystem";

    public static final String SCRIPT_ENGINE_BACKEND = "scriptEngineBackend";

}
