package es.kibu.geoapis.metrics.engine.data.conversion;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import jdk.nashorn.api.scripting.ScriptObjectMirror;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.generic.GenericRecordBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by lrodriguez2002cu on 13/10/2016.
 */
public class AvroConverter {

    Logger logger = LoggerFactory.getLogger(AvroConverter.class);

    Schema avroSchema;

    public Schema getAvroSchema() {
        return avroSchema;
    }

    public void setAvroSchema(Schema avroSchema) {
        this.avroSchema = avroSchema;
    }

    public GenericRecord convert(ScriptObjectMirror objectMirror, String metricName, String application) {

        logger.debug("Converting from object mirror metric:{} application:{}", metricName, application);
        if (avroSchema == null) {
            AvroSchemaCreator schemaCreator = new AvroSchemaCreator();
            //ScriptObjectMirror objectMirrorForObject = getEngineObjectMirrorForObject(json);
            avroSchema = schemaCreator.createSchemaFromObjectMirror(metricName, application, objectMirror);
        }
        return convert(objectMirror, avroSchema);
    }

    public List<GenericRecord> convert(List<ScriptObjectMirror> objectMirrors, String metricName, String application) {

        logger.debug("Converting from list of object mirror metric:{} application:{}", metricName, application);

        if (avroSchema == null) {
            AvroSchemaCreator schemaCreator = new AvroSchemaCreator();
            //ScriptObjectMirror objectMirrorForObject = getEngineObjectMirrorForObject(json);
            ScriptObjectMirror mirror = objectMirrors.get(0);
            avroSchema = schemaCreator.createSchemaFromObjectMirror(metricName, application, mirror);
        }

        return convert(objectMirrors);
    }

    public List<GenericRecord> convert(List<ScriptObjectMirror> objectMirrors) {
        logger.debug("Converting from list of object mirrors");

        List<GenericRecord> result = new ArrayList<>();

        for (ScriptObjectMirror objectMirror : objectMirrors) {
            GenericRecord genericRecord = convert(objectMirror, avroSchema);
            result.add(genericRecord);
        }
        return result;
    }

    private GenericRecord convert(ScriptObjectMirror objectMirror, Schema jsonSchema) {
        DataSchemaVisitor visitor = new ObjectMirrorVisitor(objectMirror);
        GenericRecordBuilder builder = new GenericRecordBuilder(jsonSchema);
        return convert(visitor, builder, jsonSchema);
    }

    private GenericRecord convert(DataSchemaVisitor visitor, GenericRecordBuilder recordBuilder, Schema schema) {

        GenericRecord recordBuilt = null;
        boolean recordFinished = false;

        if (schema == null) {
            throw new RuntimeException("schema can not be null");
        }

        while (visitor.hasNext() && !recordFinished) {
            visitor.next();

            String fieldName = visitor.getCurrentFieldName();
            DataSchemaVisitor.DataType currentDataType = visitor.getCurrentDataType();
            Object value = visitor.getCurrentValue();

            boolean isField = fieldName != null;

            switch (currentDataType) {
                case STRING:
                    //break;
                case INT:
                    //break;
                case LONG:
                    //break;
                case FLOAT:
                    //break;
                case DOUBLE:
                    //break;
                case NULL:
                    //fieldBuilder.type().nullType();
                    recordBuilder.set(fieldName, value);
                    break;
                case RECORD:
                    //logger.debug("Building a new record ");
                    Schema newSchema;
                    if (isField) {
                        logger.debug("Getting the schema for field: {} ", fieldName);
                        newSchema = schema.getField(fieldName).schema();
                    } else {
                        logger.debug("Reusing the previous record with the previous fields ", fieldName);
                        newSchema = schema;
                    }

                    GenericRecordBuilder builder = new GenericRecordBuilder(newSchema);
                    recordBuilt = convert(visitor, builder, newSchema);

                    if (isField) {
                        recordBuilder.set(fieldName, recordBuilt);
                    }
                    break;
                case END_RECORD:
                    recordBuilt = recordBuilder.build();
                    recordFinished = true;
                    break;
            }

        }
        return recordBuilt;
    }

}
