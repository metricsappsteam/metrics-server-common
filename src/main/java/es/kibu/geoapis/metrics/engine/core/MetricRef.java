package es.kibu.geoapis.metrics.engine.core;

import es.kibu.geoapis.objectmodel.Metric;

import java.io.Serializable;

/**
 * Created by lrodriguez2002cu on 25/10/2016.
 */
public class MetricRef implements Serializable {

    String application;
    String metricName;
    Metric metric;

    public String getApplication() {
        return application;
    }

    public void setApplication(String application) {
        this.application = application;
    }

    public String getMetricName() {
        return metricName;
    }

    public void setMetricName(String metricName) {
        this.metricName = metricName;
    }

    public Metric getMetric() {
        return metric;
    }

    public void setMetric(Metric metric) {
        this.metric = metric;
    }


    public MetricRef(String application, String metricName) {
        this.application = application;
        this.metricName = metricName;
    }

    public MetricRef(String application, Metric metric) {
        this.application = application;
        this.metricName = metric.getName();
        this.metric = metric;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MetricRef)) return false;

        MetricRef metricRef = (MetricRef) o;

        if (application != null ? !application.equals(metricRef.application) : metricRef.application != null)
            return false;
        return metricName != null ? metricName.equals(metricRef.metricName) : metricRef.metricName == null;
    }

    @Override
    public int hashCode() {
        int result = application != null ? application.hashCode() : 0;
        result = 31 * result + (metricName != null ? metricName.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return application + ":" + metricName;
    }
}
