package es.kibu.geoapis.metrics.engine.data.writing.core;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by lrodr_000 on 23/02/2017.
 */
public class WriterConfig {

    Map<String, Object> configKeys = new HashMap();

    public Object getConfigValue(String key) {
        return configKeys.get(key);
    }

    public void setConfigValue(String key, Object value) {
        configKeys.put(key, value);
    }

}
