package es.kibu.geoapis.metrics.engine.data.conversion;

import com.google.gson.JsonElement;

/**
 * Created by lrodr_000 on 11/10/2016.
 */
public class JsonVisitor extends MapVisitor {

    JsonElement root;

    public JsonVisitor(JsonElement root) {
        super();
        this.root = root;
    }

    @Override
    protected Object getRoot() {
        return root;
    }

    @Override
    VisitElement newVisitElement(Object o) {
        return new JsonVisitElement(o);
    }

}
