package es.kibu.geoapis.metrics.engine.evaluation;

import jdk.nashorn.internal.ir.LexicalContext;
import jdk.nashorn.internal.ir.visitor.NodeOperatorVisitor;
import jdk.nashorn.internal.ir.visitor.NodeVisitor;

/**
 * Created by lrodr_000 on 13/09/2016.
 */
public abstract class MoreComplexNodeVisitor extends NodeOperatorVisitor<LexicalContext> {

   public MoreComplexNodeVisitor() {
            super(new LexicalContext());
   }

}
