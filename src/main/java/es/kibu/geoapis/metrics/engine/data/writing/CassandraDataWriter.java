package es.kibu.geoapis.metrics.engine.data.writing;

import es.kibu.geoapis.metrics.engine.data.writing.cassandra.CassandraConfigurator;
import es.kibu.geoapis.metrics.engine.data.writing.cassandra.CassandraVariableDataWriter;
import es.kibu.geoapis.metrics.engine.data.writing.core.DataWriter;
import es.kibu.geoapis.metrics.engine.data.writing.core.WriterConfig;
import es.kibu.geoapis.objectmodel.MetricsDefinition;
import es.kibu.geoapis.objectmodel.Variable;

/**
 * Created by lrodr_000 on 24/02/2017.
 */
public abstract class CassandraDataWriter implements DataWriter {

    public static final String KEYSPACE = "keyspace";
    public static final String TABLE = "table";
    public static final String METRIC_DEFINITION = "metric_definition";
    public static final String VARIABLE = "variable";

    WriterConfig config;

    CassandraVariableDataWriter dataWriter = new CassandraVariableDataWriter();

    public CassandraDataWriter(CassandraConfigurator configurator) {
        dataWriter.setConfigurator(configurator);
    }

    protected CassandraDataWriter() {

    }

    @Override
    public void setConfig(WriterConfig config) {
        this.config = config;
    }

    public void prepare() {
        dataWriter.prepareForVariable(getVariable(), getMetricDefinition());
    }


    public Variable getVariable() {
        Variable configValue = (Variable) config.getConfigValue(VARIABLE);
        return configValue;
    }

    protected MetricsDefinition getMetricDefinition() {
        MetricsDefinition metricsDefinition = (MetricsDefinition) config.getConfigValue(METRIC_DEFINITION);
        return metricsDefinition;
    }

    protected String getVariableName() {
        Variable configValue = (Variable) config.getConfigValue(VARIABLE);
        String name = configValue.getName();
        return name;
    }


    @Override
    public WriterConfig getConfig() {
        return config;
    }


    protected CassandraWriteConfig getCassandraConfig() {
        return (CassandraWriteConfig) getConfig();
    }
}
