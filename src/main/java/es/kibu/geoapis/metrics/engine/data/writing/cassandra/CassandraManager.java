package es.kibu.geoapis.metrics.engine.data.writing.cassandra;

/**
 * Created by lrodriguez2002cu on 25/10/2016.
 */

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;
import com.fasterxml.jackson.databind.JsonNode;
import es.kibu.geoapis.converters.SchemaToCassandraTableConverter;
import es.kibu.geoapis.metrics.engine.core.ActionRef;
import es.kibu.geoapis.metrics.engine.core.MetricRef;
import es.kibu.geoapis.metrics.engine.core.MetricsContextFields;
import es.kibu.geoapis.metrics.engine.data.writing.NamingConventions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static es.kibu.geoapis.metrics.engine.results.ResultCommons.METRIC_TIME;

public class CassandraManager {

    Logger logger = LoggerFactory.getLogger(CassandraManager.class);

    final CassandraSettings settings;

    Session session;

    static CassandraManager _instance;

    public static CassandraManager getInstance(CassandraConfigurator configurator) {

        if (_instance == null) {
            CassandraSettings settings = configurator.getSettings(null);
            _instance = new CassandraManager(settings);
        }
        return _instance;
    }

    public static CassandraManager getInstance(CassandraSettings settings) {
        if (_instance == null) {
            _instance = new CassandraManager(settings);
        }
        return _instance;
    }


    public static CassandraManager getExistingInstance() {
        if (_instance == null) {
            throw new RuntimeException("The instance must be initialized fist");
        }
        return _instance;
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    String keyspace;

    public CassandraManager(CassandraSettings settings) {
        this.settings = settings;

        init();
    }

    public String getKeyspace() {
        return keyspace;
    }

    public void setKeyspace(String keyspace) {
        this.keyspace = keyspace;
    }

    public String getCassandraHost() {
        return cassandraHost;
    }

    public void setCassandraHost(String cassandraHost) {
        this.cassandraHost = cassandraHost;
    }

    String cassandraHost;

    public void init() {
        //Query
        keyspace = settings.getCassandraDBName();/*CASSANDRA_DB_NAME*/

        String query = String.format("CREATE KEYSPACE IF NOT EXISTS %s WITH REPLICATION ", keyspace)
                + String.format("= {'class':'SimpleStrategy', 'replication_factor': %d};", settings.getReplicationFactor());

        logger.debug("create query: {}", query);

        cassandraHost = settings.getCassandraDBUri()/*CASSANDRA_DB_URI*/;
        String[] hosts = cassandraHost.split(",");
        Cluster cluster = Cluster.builder().addContactPoints(hosts).build();
        
        //Creating Session object
        session = cluster.connect();

        //Executing the query
        session.execute(query);

        //using the KeySpace
        session.execute(String.format("USE %s", keyspace));
        logger.debug("Keyspace created: {}", keyspace);
    }

    public static String getCassandraTableNameFor(MetricRef metricRef) {
        return NamingConventions.getCassandraTableNameFor(metricRef);
    }

    public static String getCassandraTableNameFor(ActionRef actionRef) {
        return NamingConventions.getCassandraTableNameFor(actionRef);
    }

    public static class CassandraManagerConfig {

        public List<String> getPrimaryKeyFields() {
            return primaryKeyFields;
        }

        public void setPrimaryKeyFields(List<String> primaryKeyFields) {
            this.primaryKeyFields = primaryKeyFields;
        }

        List<String> primaryKeyFields;
        List<String> clusteringFields;

         /*Map<String, String> registeredTypesForProperties = new HashMap<>();*/

        public CassandraManagerConfig(String keyspace) {
            List<String> fields = new ArrayList<>();
            fields.add(METRIC_TIME);
            fields.add(MetricsContextFields.APPLICATION_ID);
            fields.add(MetricsContextFields.USER_ID);
            fields.add(MetricsContextFields.SESSION_ID);
            primaryKeyFields = fields;
            this.keyspace = keyspace;
        }

      /*   public void registerTypeForProperty(String propertyName, String propertyType){
             registeredTypesForProperties.put(propertyName, propertyType);
         }

         public Map<String,  String> getRegisteredTypesForProperties() {
             return registeredTypesForProperties;
         }

         public void setRegisteredTypesForProperties(Map<String,  String>  typesForProperties) {
             registeredTypesForProperties = typesForProperties;
         }*/

        private Map<String, String> extraProperties = new HashMap<>();

        public Map<String, String> getExtraProperties() {
            return extraProperties;
        }

        public void setExtraProperties(Map<String, String> extraProperties) {
            this.extraProperties = extraProperties;
        }

        String keyspace;

        public String getKeyspace() {
            return keyspace;
        }

        public void setKeyspace(String keyspace) {
            this.keyspace = keyspace;
        }

        //TODO: make this configurable through the settingsImpl
        boolean useFrozen = true;

        public boolean useFrozen() {
            return useFrozen;
        }

        public void useFrozen(boolean useFrozen) {
            this.useFrozen = useFrozen;
        }

    }

    public void createCassandraTable(String cassandraTableNameFor, JsonNode node, CassandraManagerConfig config) {
        SchemaToCassandraTableConverter.CassandraTableStatementsGenerator generator =
                new SchemaToCassandraTableConverter.CassandraTableStatementsGenerator(cassandraTableNameFor,
                        config.getKeyspace(),
                        config.getPrimaryKeyFields(), config.getExtraProperties(), config.useFrozen());
        List<String> statements = generator.generateFromJson(node);

        //List<String> indexesStatements = createIndexes(config, cassandraTableNameFor);

        //creation statements
        executeStatements(statements);
        //indexes statements
        //executeStatements(indexesStatements);

    }

    private List<String> createIndexes(CassandraManagerConfig config, String tableName) {
        List<String> indexesStatements = new ArrayList<>();
        for (String field : config.getPrimaryKeyFields()) {
            if (field.equalsIgnoreCase(MetricsContextFields.APPLICATION_ID) ||
                    field.equalsIgnoreCase(MetricsContextFields.USER_ID) ||
                    field.equalsIgnoreCase(MetricsContextFields.SESSION_ID)) {

                String statement = indexStatement(field, config.getKeyspace(), tableName);
                indexesStatements.add(statement);
            }
        }

        return indexesStatements;
    }

    private void executeStatements(List<String> statements) {
        logger.debug("executing multiple statements: {}", statements);
        for (String statement : statements) {
            executeStatement(statement);
        }
    }

    public void executeStatement(String query) {
        logger.debug("Executing statement: {}", query);
        session.execute(query);
    }

    public String insertStatement(String keyspace, String tableName, String jsonString) {
        String statement = String.format("INSERT INTO \"%s\".\"%s\" JSON '%s' ;", keyspace, tableName, jsonString);
        return statement;
    }

    public String indexStatement(String indexField, String keyspace, String tableName) {
/*
        CREATE INDEX indexApp ON metrics."var_movementspeedMetricsNacho" (application);
        CREATE INDEX indexSession ON metrics."var_movementspeedMetricsNacho" (session);
        CREATE INDEX indexUser ON metrics."var_movementspeedMetricsNacho" (session);
*/
        String statement = String.format("CREATE INDEX IF NOT EXISTS index%s ON \"%s\".\"%s\" (%s);", indexField, keyspace, tableName, indexField);
        return statement;
    }


    public void insert(String keyspace, String tableName, String json) {
        String insertStatement = insertStatement(keyspace, tableName, json);
        logger.debug("About to execute: \n keyspace: {}, \n tablename: {},\n statement: {}", keyspace, tableName, insertStatement);
        executeStatement(insertStatement);
    }


}




