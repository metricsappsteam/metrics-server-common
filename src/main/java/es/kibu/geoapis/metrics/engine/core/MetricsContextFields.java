package es.kibu.geoapis.metrics.engine.core;

/**
 * Created by lrodr_000 on 24/02/2017.
 */
public class MetricsContextFields {
    public static final String APPLICATION_ID = "application";
    public static final String SESSION_ID = "session";
    public static final String TIME_ID = "timeid";
    public static final String EXEC_ID = "execid";
    public static final String USER_ID = "user";

}
