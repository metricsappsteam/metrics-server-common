package es.kibu.geoapis.metrics.engine.results;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.fge.jackson.JsonLoader;
import es.kibu.geoapis.converters.FormatsSupported;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static es.kibu.geoapis.metrics.engine.core.MetricsContextFields.*;
import static es.kibu.geoapis.metrics.engine.results.ResultCommons.METRIC_TIME;
import static es.kibu.geoapis.metrics.engine.results.ResultCommons.VALUE;

/**
 * Created by lrodr_000 on 27/10/2016.
 */
public class ResultUtils {


    public static final String TYPE = "type";
    public static final String FORMAT = "format";
    public static final String STRING = "string";
    public static final String PROPERTIES = "properties";
    public static final String OBJECT = "object";
    public static final String ADDITIONAL_PROPERTIES = "additionalProperties";

    private static Logger logger = LoggerFactory.getLogger(ResultUtils.class);


    public static JsonNode addNoAdditionalProperties(JsonNode outSchema) {
        List<JsonNode> listToFix = new ArrayList<>();
        addNoAdditionalProperties(outSchema, listToFix);
        for (JsonNode jsonNode : listToFix) {
            fixObject((ObjectNode) jsonNode);
        }
        return outSchema;
    }

    public static JsonNode addNoAdditionalProperties(JsonNode outSchema, List<JsonNode> listToFix) {
        Iterator<JsonNode> elements = outSchema.elements();
        if (outSchema instanceof ObjectNode) {
            ObjectNode jsonNodes = collectFixObject((ObjectNode) outSchema);
            listToFix.add(jsonNodes);
        }
        while (elements.hasNext()) {

            JsonNode next = elements.next();
            if (next instanceof ObjectNode) {
                addNoAdditionalProperties(next, listToFix);
            } else if (next instanceof ArrayNode) {
                ArrayNode arrayNode = (ArrayNode) next;
                addNoAdditionalProperties(arrayNode, listToFix);
            }
        }
        return outSchema;
    }

    private static void fixObject(ObjectNode next) {
        ObjectNode nextObj = next;
        if (nextObj.has(TYPE)
                && nextObj.get(TYPE).asText().equals(OBJECT)
                ) {
            //if (!nextObj.has("additionalProperties")) {
            nextObj.put("additionalProperties", false);
            //}
        }
    }

    private static ObjectNode collectFixObject(ObjectNode next) {
        ObjectNode nextObj = next;
        if (nextObj.has(TYPE)
                && nextObj.get(TYPE).asText().equals(OBJECT)
                /*&& !nextObj.has("additionalProperties")*/) {
            //nextObj.put("additionalProperties",  true);
            return nextObj;
        }
        return nextObj;
    }


    public static boolean isWrapped(JsonNode node) {
        JsonNode properties = node.get(PROPERTIES);
        return node.has(PROPERTIES) && properties.has(VALUE) && properties.has(ResultCommons.METRIC_TIME);
    }

    public static JsonNode wrapValueSchema(JsonNode outSchema) {
        if (!isWrapped(outSchema)) {
            ObjectNode objectNode = JsonNodeFactory.instance.objectNode();

            ObjectNode properties = objectNode.put(TYPE, OBJECT)
                    .putObject(PROPERTIES);

            properties.set(VALUE, outSchema);
            ObjectNode timeNode = properties.putObject(METRIC_TIME);
            timeNode.put(TYPE, STRING);
            timeNode.put(FORMAT, FormatsSupported.DATE_TIME.toFormatString());

            properties.putObject(APPLICATION_ID).put(TYPE, STRING);
            properties.putObject(USER_ID).put(TYPE, STRING);
            properties.putObject(SESSION_ID).put(TYPE, STRING);

            ObjectNode timeIdObject = properties.putObject(TIME_ID);
            timeIdObject.put(TYPE, STRING);
            timeIdObject.put(FORMAT, FormatsSupported.TIMEUUID.toFormatString());

            ObjectNode execIdObj = properties.putObject(EXEC_ID);
            execIdObj.put(TYPE, STRING);
            execIdObj.put(FORMAT, FormatsSupported.TIMEUUID.toFormatString());


            objectNode.put(ADDITIONAL_PROPERTIES, false);


            return addNoAdditionalProperties(objectNode);
        } else {
            return addNoAdditionalProperties(outSchema);
        }
    }

    /*public static JsonNode wrapValueSchema1(JsonNode outSchema) throws IOException {
        if (!isWrapped(outSchema)) {
            ObjectNode objectNode = (ObjectNode) JsonLoader.fromResource("/samples/schemas/result-schema.json");
            if (outSchema.isObject()) {
                logger.debug("setting  the id:");
                // ((ObjectNode) outSchema).put("id", "id"*//*JsonNodeFactory.instance.textNode("id")*//*);
            }

            *//*JsonNode jsonNode = *//*
            ((ObjectNode) objectNode.get("definitions")).set("value_def", outSchema);

            logger.debug("Full Object: {}", objectNode);
            return addNoAdditionalProperties(objectNode);
        } else {
            return addNoAdditionalProperties(outSchema);
        }
    }
*/
    public static JsonNode wrapOutputSchema(String outputSchema) throws IOException {
        JsonNode outSchema = JsonLoader.fromString(outputSchema);
        return wrapValueSchema(outSchema);
    }
}
