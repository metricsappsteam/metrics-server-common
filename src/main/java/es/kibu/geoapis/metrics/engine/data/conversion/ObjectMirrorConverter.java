package es.kibu.geoapis.metrics.engine.data.conversion;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import jdk.nashorn.api.scripting.ScriptObjectMirror;

import java.util.Map;

/**
 * Created by lrodr_000 on 31/10/2016.
 */
public class ObjectMirrorConverter {

    public static String convertToJsonGson(ScriptObjectMirror objectMirror) {
        Gson gson = new GsonBuilder().serializeSpecialFloatingPointValues().create();
        String json = gson.toJson((Map) objectMirror);
        return json;
    }


    public static String convertToJson(ScriptObjectMirror objectMirror) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        //Object to JSON in String
        String json= mapper.writeValueAsString((Map)objectMirror);
        return json;
    }
}
