package es.kibu.geoapis.metrics.engine.data.conversion;

import com.google.gson.JsonElement;

import java.util.Iterator;
import java.util.Map;

/**
 * Created by lrodr_000 on 11/10/2016.
 */
public interface DataSchemaVisitor<E> extends Iterator<E> {

    //enum ItemType { /*RECORD,*/FIELD}

    enum DataType {STRING, INT, LONG, FLOAT, NULL, DOUBLE, RECORD, END_RECORD, BOOLEAN}

    //ItemType getCurrentType();

    String getCurrentFieldName();

    DataType getCurrentDataType();

    Object getCurrentValue();

    E next();

    boolean hasNext();


    abstract class VisitElement {

        protected Object value;
        protected DataType dataType;
        protected String name;


        public Object getValue() {
            return value;
        }

        public void setValue(Object value) {
            this.value = value;
        }

        public DataType getDataType() {
            return dataType;
        }

        public String getName() {
            return name;
        }

        public static VisitElement END_RECORD() {
            VisitElement visitElement = new VisitElement(){};
            visitElement.dataType = DataType.END_RECORD;
            return visitElement;
        }

        @Override
        public String toString() {
            return "VisitElement{" +
                    " name='" + name + '\'' +
                    ", itemType=" + value +
                    ", dataType=" + dataType +
                    '}';
        }

        protected VisitElement() {
        }

        protected VisitElement(Object elem) {
            if (elem instanceof Map.Entry) {
                Map.Entry entry = (Map.Entry) elem;
                name = ((String) entry.getKey());
                dataType = getType(entry.getValue());
                value = entry.getValue();
            }
        }

        protected DataType getType(Object value) {
            return null;
        }
    }
}
