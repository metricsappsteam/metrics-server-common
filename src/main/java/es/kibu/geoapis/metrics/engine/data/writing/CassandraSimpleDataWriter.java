package es.kibu.geoapis.metrics.engine.data.writing;

import es.kibu.geoapis.metrics.engine.data.writing.cassandra.CassandraConfigurator;
import es.kibu.geoapis.metrics.engine.data.writing.core.WriterConfig;
import es.kibu.geoapis.objectmodel.MetricsDefinition;
import es.kibu.geoapis.objectmodel.Variable;

/**
 * Created by lrodr_000 on 09/03/2017.
 */
public class CassandraSimpleDataWriter extends CassandraDataWriter {

    public CassandraSimpleDataWriter(CassandraConfigurator configurator) {
        super(configurator);
    }

    @Override
    public void setConfig(WriterConfig config) {
        super.setConfig(config);
        prepare();
    }

    @Override
    public void write(String jsonContent, MetricsDefinition metricsDefinition) throws Exception {
        Variable variable = metricsDefinition.getVariableByName(getVariableName());
        if (variable != null) {
            //CassandraWriteConfig config = getCassandraConfig();
            //NamingConventions.overrideVariableTableName(config.getTableName());
            try {
                dataWriter.write(variable, jsonContent);
            } finally {
                //NamingConventions.stopOverrideVariableTableName();
            }
        }
    }

}
