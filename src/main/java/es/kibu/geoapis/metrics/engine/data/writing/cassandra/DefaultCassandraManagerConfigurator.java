package es.kibu.geoapis.metrics.engine.data.writing.cassandra;

/*import es.kibu.geoapis.metrics.config.ConfigUtils;
import es.kibu.geoapis.metrics.config.SettingsImpl;*/

/**
 * Created by lrodr_000 on 24/02/2017.
 */
public class DefaultCassandraManagerConfigurator implements CassandraConfigurator {

    CassandraSettings settings;

    public DefaultCassandraManagerConfigurator(CassandraSettings settings) {
       this.settings = settings;
    }

    @Override
    public CassandraSettings getSettings(Object settings) {
       if (settings == null) return this.settings;
       if (settings instanceof  CassandraSettings) return (CassandraSettings) settings;
       return null;
    }

   /* public static CassandraSettings fromSettingsImpl(SettingsImpl settingsProvider) {

        return new CassandraSettings() {

            @Override
            public String getCassandraDBUser() {
                return settingsProvider.CASSANDRA_DB_USER;
            }

            @Override
            public String getCassandraDBPass() {
                return settingsProvider.CASSANDRA_DB_PASS;
            }

            @Override
            public String getCassandraDBName() {
                return settingsProvider.CASSANDRA_DB_NAME;
            }

            @Override
            public String getCassandraDBUri() {
                return settingsProvider.CASSANDRA_DB_URI;
            }

            @Override
            public void setCassandraDBName(String cassandraDBName) {

            }

            @Override
            public void setCassandraDBUri(String cassandraDBUri) {

            }
        };
    }*/

}
