package es.kibu.geoapis.metrics.engine.data.writing.cassandra;

import com.fasterxml.jackson.databind.JsonNode;
import es.kibu.geoapis.converters.SchemaToCassandraTableConverter;
import es.kibu.geoapis.metrics.engine.core.MetricsContextFields;
import es.kibu.geoapis.metrics.engine.data.DataException;
import es.kibu.geoapis.metrics.engine.data.conversion.ObjectMirrorConverter;
import es.kibu.geoapis.metrics.engine.data.writing.MetricOutputWriter;
import es.kibu.geoapis.metrics.engine.data.writing.WriteNotifier;
import es.kibu.geoapis.metrics.engine.results.ResultUtils;
import jdk.nashorn.api.scripting.ScriptObjectMirror;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by lrodriguez2002cu on 18/01/2017.
 *
 * A generic class that is able to create the table and write the data.
 */
public abstract class GenericCassandraOutputWriter<R> extends BaseCassandraWriter implements MetricOutputWriter<Object>, WriteNotifier {

    protected R reference;

    Logger logger = LoggerFactory.getLogger(CassandraMetricOutputWriter.class);

    public GenericCassandraOutputWriter(R reference) {
        this.reference = reference;
        assert reference != null;
    }

    protected void createTableIfNotExist() {
        String cassandraTableName = getCassandraTableName();
        try {
            JsonNode schemaWrappedJsonNode = ResultUtils.wrapOutputSchema(getOutputSchema());

            CassandraManager.CassandraManagerConfig config = new CassandraManager.CassandraManagerConfig(CassandraManager.getInstance(getConfigurator()).getKeyspace());

            Map<String, String> extraProperties = new HashMap<>();
            extraProperties.put(MetricsContextFields.TIME_ID, SchemaToCassandraTableConverter.CassandraTypes.TIMEUUID.getTypeName());
            extraProperties.put(MetricsContextFields.EXEC_ID, SchemaToCassandraTableConverter.CassandraTypes.TIMEUUID.getTypeName());
            config.setExtraProperties(extraProperties);

            createTableIfNotExists(cassandraTableName, schemaWrappedJsonNode, config);
        } catch (IOException e) {
            logger.error("Error while executing creating new table", e);
            String message = this.reference.toString();
            throw new DataException(String.format("Handler failed to execute action: %s, ref: %s", "createTableIfNotExist", message), e);
        }
    }

    protected abstract String getCassandraTableName();

    protected abstract String getOutputSchema();

    @Override
    public void write(Object mirror) throws IOException {
        //mirror.
        CassandraManager existingInstance = getCassandraManager();

        String tableName = getCassandraTableName();
        String json = getJson(mirror);
        String keyspace = getKeyspace();

        existingInstance.insert(keyspace, tableName, json);
        notifyWrite();

    }

    private String getJson(Object mirror) {
        String json = null;
        if (mirror instanceof ScriptObjectMirror) {
           json = ObjectMirrorConverter.convertToJsonGson((ScriptObjectMirror)mirror);
        }else if (mirror instanceof  String ){
            //assume this string is a json conformant string
            json = (String)mirror;
        }
        return json;
    }


    @Override
    public void write(List<Object> mirrorList) throws IOException {

        CassandraManager existingInstance = getCassandraManager();
        String tableName = getCassandraTableName();
        String keyspace = getKeyspace();

        for (Object objectMirror : mirrorList) {
            String json = getJson(objectMirror);//ObjectMirrorConverter.convertToJsonGson(objectMirror);
            existingInstance.insert(keyspace, tableName, json);
            notifyWrite();
        }
    }

    @Override
    public Object getOutputLocation() {
        return null;
    }


    @Override
    public void notifyWrite() {

    }
}
