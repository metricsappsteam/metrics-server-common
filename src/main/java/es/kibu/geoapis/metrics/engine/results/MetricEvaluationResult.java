package es.kibu.geoapis.metrics.engine.results;

//import es.kibu.geoapis.metrics.engine.EngineUtils;
import es.kibu.geoapis.objectmodel.Metric;

import java.io.Serializable;

/**
 * Created by lrodriguez2002cu on 07/09/2016.
 */
public class MetricEvaluationResult implements Serializable {

    Object result;

    Metric metric;

    boolean failed;

    public boolean isFailed() {
        return failed;
    }

    public void setFailed(boolean failed) {
        this.failed = failed;
    }

    public Metric getMetric() {
        return metric;
    }

    public Object getResult() {
        return result;
    }

    public MetricEvaluationResult(Metric metric, Object result) {
        this.metric = metric;
        this.result = result;
        failed = result instanceof Exception;
    }

    public Long getAsLong() {
        return getAs();
    }

    public Integer getAsInt() {
        return getAs();
    }

    public <T> T getAs() {
       // return (T) EngineUtils.unwrapMetricResultObject(this.result);
        throw new RuntimeException("This conversion is not implemented after refactoring");
    }

    public String getAsString() {
        return getAs();
    }

    public Double getAsDouble() {
        return getAs();
    }

    public Exception getAsError() {
        return getAs();
    }

    @Override
    public String toString() {
        return "MetricEvaluationResult{" +
                "result=" + result +
                ", metric=" + metric +
                ", failed=" + failed +
                '}';
    }
}
