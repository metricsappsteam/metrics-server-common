package es.kibu.geoapis.metrics.engine.data.conversion;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ListProcessingReport;
import com.github.fge.jsonschema.core.report.LogLevel;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.core.tree.CanonicalSchemaTree;
import com.github.fge.jsonschema.core.tree.SchemaTree;
import com.github.fge.jsonschema.core.tree.key.SchemaKey;
import com.github.fge.jsonschema.core.util.ValueHolder;
import com.github.fge.jsonschema2avro.AvroWriterProcessor;
import com.google.gson.JsonElement;
import es.kibu.geoapis.metrics.engine.data.DataException;
import es.kibu.geoapis.metrics.engine.results.MetricEvaluationResult;
import es.kibu.geoapis.objectmodel.Metric;
import es.kibu.geoapis.serialization.Utils;
import jdk.nashorn.api.scripting.ScriptObjectMirror;
import org.apache.avro.Schema;
import org.apache.avro.SchemaBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

import static es.kibu.geoapis.metrics.engine.results.ResultUtils.wrapOutputSchema;

/**
 * Created by lrodriguez2002cu on 13/10/2016.
 */
public class AvroSchemaCreator {

    static Logger logger = LoggerFactory.getLogger(AvroSchemaCreator.class);


    public boolean isAllowNulls() {
        return allowNulls;
    }

    public void setAllowNulls(boolean allowNulls) {
        this.allowNulls = allowNulls;
    }

    boolean allowNulls = true;


    public Schema createSchema(String metricName, String application, DataSchemaVisitor visitor) {
        String namespace = "es.kibu.metric-engine.data";
        String basenamespace = String.format("%s.%s.%s", namespace, application, metricName);
        SchemaBuilder.TypeBuilder builder = SchemaBuilder.builder(basenamespace);
        SchemaBuilder.RecordBuilder recordBuilder = builder.record(basenamespace);
        Schema schema = buildSchemaForRecord(visitor, recordBuilder, recordBuilder.fields(), basenamespace);
        return schema;
    }

    public Schema createSchemaFromJson(String metricName, String application, JsonElement template) {
        JsonVisitor visitor = new JsonVisitor(template);
        Schema schema = createSchema(metricName, application, visitor);
        return schema;
    }

    public Schema createSchemaFromObjectMirror(String metricName, String application, ScriptObjectMirror template) {
        DataSchemaVisitor<DataSchemaVisitor.VisitElement> visitor = new ObjectMirrorVisitor(template);
        Schema schema = createSchema(metricName, application, visitor);
        return schema;
    }

    public Schema createSchemaFromMetricResult(String metricName, String application, MetricEvaluationResult template) {
        if (template.getResult() instanceof ScriptObjectMirror) {
            DataSchemaVisitor<DataSchemaVisitor.VisitElement> visitor = new ObjectMirrorVisitor((ScriptObjectMirror) template.getResult());
            Schema schema = createSchema(metricName, application, visitor);
            return schema;
        } else throw new RuntimeException("Unrecognized result type");
    }

    public static class SchemaCreationException extends DataException {
        public SchemaCreationException(String message) {
            super(message);
        }
    }

    public Schema createSchemaFromMetric(Metric metric) throws ProcessingException, IOException {
        String outputSchema = metric.getOutput().getSchema();
        return createSchemaFromJsonSchema(outputSchema);
    }

    public static Schema createSchemaFromJsonSchema(String outputSchema) {

        try {
            JsonNode jsonSchema = wrapOutputSchema(outputSchema);

            logger.debug("wrapped schema: {}", jsonSchema);

            //the schema must be valid.
            Utils.checkJsonSchemaValid(jsonSchema);

            final SchemaTree tree = new CanonicalSchemaTree(SchemaKey.anonymousKey(), jsonSchema);
            final ValueHolder<SchemaTree> input = ValueHolder.hold("schema", tree);

            //final Schema expected = new Schema.Parser().parse(avro.toString());
            AvroWriterProcessor processor = new AvroWriterProcessor();
            ProcessingReport report = new ListProcessingReport(LogLevel.DEBUG);

            final Schema avroSchema = processor.process(report, input).getValue();
            return avroSchema;
        } catch (IOException | ProcessingException e) {
            throw new DataException("Problems while creating the avro schema", e);
        }
    }



    private Schema buildSchemaForRecord(DataSchemaVisitor visitor, SchemaBuilder.RecordBuilder recordBuilder, SchemaBuilder.FieldAssembler fields, String namespace) {

        Schema schemaBuilt = null;
        boolean recordFinished = false;

        while (visitor.hasNext() && !recordFinished) {
            visitor.next();

            String fieldName = visitor.getCurrentFieldName();
            DataSchemaVisitor.DataType currentDataType = visitor.getCurrentDataType();
            SchemaBuilder.FieldBuilder fieldBuilder = null;

            boolean isField = (fieldName != null);

            fieldBuilder = isField ? fields.name(fieldName) : null;

            switch (currentDataType) {
                case STRING:
                    stringType(fieldBuilder);
                    break;
                case INT:
                    intType(fieldBuilder);
                    break;
                case LONG:
                    longType(fieldBuilder);
                    break;
                case FLOAT:
                    floatType(fieldBuilder);
                    break;
                case DOUBLE:
                    doubleType(fieldBuilder);
                    break;
                case NULL:
                    //nullType(fieldBuilder);
                    throw new SchemaCreationException(
                            String.format("The field '%s' is null, and therefore not representative for creating the schema",
                                    fieldName));
                    //break;
                case RECORD:
                    logger.debug("Building a new record ");
                    SchemaBuilder.RecordBuilder builder;
                    SchemaBuilder.FieldAssembler recordFields;

                    String newNamespace = namespace;

                    if (isField) {
                        logger.debug("Creating a new record: {} ", fieldName);
                        newNamespace += "." + fieldName;
                        SchemaBuilder.TypeBuilder typeBuilder = SchemaBuilder.builder(newNamespace);
                        builder = typeBuilder.record(fieldName);
                        recordFields = builder.fields();
                    } else {
                        logger.debug("Reusing the previous record with the previous fields ", fieldName);
                        builder = recordBuilder;
                        recordFields = fields;
                    }

                    schemaBuilt = buildSchemaForRecord(visitor, builder, recordFields, newNamespace);
                    if (fieldBuilder != null) {
                        fieldBuilder.type(schemaBuilt).noDefault();
                    }
                    break;
                case END_RECORD:
                    schemaBuilt = (Schema) fields.endRecord();
                    recordFinished = true;
                    break;
            }

        }
        return schemaBuilt;
    }

    private void nullType(SchemaBuilder.FieldBuilder fieldBuilder) {
        fieldBuilder.type().nullType();
    }

    private void doubleType(SchemaBuilder.FieldBuilder fieldBuilder) {
        if (allowNulls) {
            fieldBuilder.type().nullable().doubleType().noDefault();
        } else {
            fieldBuilder.type().doubleType().noDefault();
        }
    }

    private void floatType(SchemaBuilder.FieldBuilder fieldBuilder) {
        if (allowNulls) {
            fieldBuilder.type().nullable().floatType().noDefault();
        } else {
            fieldBuilder.type().floatType().noDefault();
        }
    }

    private void longType(SchemaBuilder.FieldBuilder fieldBuilder) {
        if (allowNulls) {
            fieldBuilder.type().nullable().longType().noDefault();
        } else {
            fieldBuilder.type().longType().noDefault();
        }
    }

    private void intType(SchemaBuilder.FieldBuilder fieldBuilder) {
        if (allowNulls) {
            fieldBuilder.type().nullable().intType().noDefault();

        } else {
            fieldBuilder.type().intType().noDefault();
        }
    }

    private void stringType(SchemaBuilder.FieldBuilder fieldBuilder) {
        if (allowNulls) {
            fieldBuilder.type().nullable().stringType().noDefault();
        } else
            fieldBuilder.type().stringType().noDefault();
    }

}

