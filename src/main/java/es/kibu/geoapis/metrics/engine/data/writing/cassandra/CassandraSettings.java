package es.kibu.geoapis.metrics.engine.data.writing.cassandra;

/**
 * Created by lrodr_000 on 24/02/2017.
 */
public interface CassandraSettings {
    String getCassandraDBUser();

    String getCassandraDBPass();

    String getCassandraDBName();

    String getCassandraDBUri();

    void setCassandraDBName(String cassandraDBName);

    void setCassandraDBUri(String cassandraDBUri);

    int getReplicationFactor();
}
