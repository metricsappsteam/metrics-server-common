package es.kibu.geoapis.metrics.engine.data;

/**
 * Created by lrodr_000 on 19/10/2016.
 */
public class DataException extends RuntimeException {

    public DataException(String message) {
        super(message);
    }

    public DataException(String message, Throwable cause) {
        super(message, cause);
    }
}
