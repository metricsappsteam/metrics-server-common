package es.kibu.geoapis.metrics.engine.data.writing;

import java.io.IOException;
import java.util.List;

/**
 * Created by lrodriguez2002cu on 14/10/2016.
 */
public interface MetricOutputWriter<T> {

    void write(T mirror) throws IOException;

    void write(List<T> mirror) throws IOException;

    Object getOutputLocation();

}
