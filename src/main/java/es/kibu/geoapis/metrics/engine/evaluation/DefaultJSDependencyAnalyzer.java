package es.kibu.geoapis.metrics.engine.evaluation;

import jdk.nashorn.internal.ir.*;
import jdk.nashorn.internal.parser.Parser;
import jdk.nashorn.internal.runtime.Context;
import jdk.nashorn.internal.runtime.ErrorManager;
import jdk.nashorn.internal.runtime.Source;
import jdk.nashorn.internal.runtime.options.Options;
import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DefaultDirectedGraph;

import java.util.List;

/**
 * Created by lrodriguez2002cu on 08/09/2016.
 * #tesis
 * This class analyzes the dependencies between the different variables and functions declared in the metrics script.
 * The purpose of this class is to help later to schedule the update of the different metrics calculation. Based on the
 * ideas that we have now, not all the metrics defined now are to be recalculated each time the data is updated.
 * In principle, only those metrics that use a variable directly are supposed to be calculated. But it is would be possible
 * to consider all depending on metrics who directly uses the variable and so on..
 * The DependencyAnalyzer uses nashorn parsing api for determining all the dependencies, and can build a dependency graph,
 * with some of the needed variables, it would allow to help considering only those needed (i.e the variables declared in
 * the metrics definition file).
 *
 *
 */

public class DefaultJSDependencyAnalyzer implements DependencyAnalyzer {

    DependencyExtractionVisitor visitor = new DependencyExtractionVisitor();

    String script;

    DependencyExtractionVisitor.OverflowDetector detector;

    public void analyze() {
        parse(script);
    }

    private void parse(String sourceCode) {

        Options options = new Options("nashorn");
        options.set("anon.functions", true);
        options.set("parse.only", true);
        options.set("scripting", true);


        ErrorManager errors = new ErrorManager();
        Context context = new Context(options, errors, Thread.currentThread().getContextClassLoader());
        /*"var a = 10; var b = a + 1; var fn = function () { return a + b}; " +
                "function someFunction() { var b = 0 ; return b + 1; }  "*/
        Source source = Source.sourceFor("metricsScript", sourceCode);

        Parser parser = new Parser(context.getEnv(), source, errors);

        FunctionNode functionNode = parser.parse();

        visitor.visit(functionNode);

    }

    public DefaultJSDependencyAnalyzer(String script) {
        this.script = script;
    }

    public DependencyExtractionVisitor.Scope findFunction(String functionName) {
        return visitor.findFunction(functionName);
    }

    @Override
    public UsageStats uses(String varUsed, String varUser) {
        detector = new DependencyExtractionVisitor.OverflowDetector();
        stats = new UsageStats(varUsed);
        DependencyExtractionVisitor.Scope scope = findFunction(varUser);
        uses(varUsed, scope);
        return stats;
    }

    UsageStats stats;

    private boolean uses(String varUsed, DependencyExtractionVisitor.Scope scope) {

        if (scope != null) {
            stats.incDep(scope);

            if ((detector.exists(scope.name))) {
                stats.setCicleInfo(scope.name, scope.getScopeList());
                stats.decDep(scope);
                return false;
            }

            DependencyExtractionVisitor.UsageDescriptor usageDescriptor = scope.usesExternal(varUsed);
            if (usageDescriptor != null && usageDescriptor.isUsed()) {
                if (!usageDescriptor.usedDirectly()) {
                    stats.incDep(usageDescriptor.getSequence());
                }
                return true;
            }

            List<DependencyExtractionVisitor.Scope> calledScopes = scope.calls();
            if (calledScopes != null) {
                detector.add(scope);
                if (uses(varUsed, calledScopes)) return true;
            }

            //search the children
            List<DependencyExtractionVisitor.Scope> childScopes = scope.getChildScopes();
            if (uses(varUsed, childScopes)) return true;

            stats.decDep(scope);
        }

        return false;
    }

    private boolean uses(String varUsed, List<DependencyExtractionVisitor.Scope> childScopes) {
        for (DependencyExtractionVisitor.Scope childScope : childScopes) {
            boolean uses = /*(detector.exists(childFunction.name))?false: */uses(varUsed, childScope);
            if (uses) {
                return true;
            } else {
                //stats.decDep(childFunction);
            }
        }
        return false;
    }


    public List<DependencyExtractionVisitor.Scope> getFunctionsList() {
        return visitor.getFunctionsList();
    }

    @Override
    public DirectedGraph<String, UsageStats> buildUsageGraphFor(List<String> variablesOrFunctions) {

        //validate that the variables exists
        //TODO: visitor.findFunction()

        /**the usage graph indicates who uses a given variable*/
        DirectedGraph<String, UsageStats> usageGraph =
                new DefaultDirectedGraph<>(UsageStats.class);

        for (String variablesOrFunctionUsed : variablesOrFunctions) {
            for (String variablesOrFunctionUser : variablesOrFunctions) {

                if (!usageGraph.containsVertex(variablesOrFunctionUsed)) usageGraph.addVertex(variablesOrFunctionUsed);
                if (!usageGraph.containsVertex(variablesOrFunctionUser)) usageGraph.addVertex(variablesOrFunctionUser);

                UsageStats usageStats = uses(variablesOrFunctionUsed, variablesOrFunctionUser);
                UsageStats usage = usageStats;
                if (usage.isUsed()) {
                    usageGraph.addEdge(variablesOrFunctionUsed, variablesOrFunctionUser, usage);
                }
            }
        }

        return usageGraph;
    }

}
