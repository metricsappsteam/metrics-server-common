package es.kibu.geoapis.metrics.engine.data.writing;

import es.kibu.geoapis.metrics.engine.data.writing.cassandra.CassandraConfigurator;
import es.kibu.geoapis.metrics.engine.data.writing.core.WriterConfig;
import es.kibu.geoapis.objectmodel.MetricsDefinition;
import es.kibu.geoapis.objectmodel.Variable;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.util.List;

/**
 * Created by lrodr_000 on 09/03/2017.
 */
public class CassandraBulkDataWriter extends CassandraDataWriter {

    public CassandraBulkDataWriter(CassandraConfigurator configurator) {
        super(configurator);
    }

    public CassandraBulkDataWriter() {
        super();
    }

    @Override
    public void setConfig(WriterConfig config) {
        try {
            super.setConfig(config);
            NamingConventions.overrideVariableTableName(getCassandraConfig().getTableName());
            prepare();
        } finally {
            NamingConventions.stopOverrideVariableTableName();
        }
    }

    @Override
    public void write(String jsonFile, MetricsDefinition metricsDefinition) throws Exception {
        Variable variable = metricsDefinition.getVariableByName(getVariableName());
        if (variable != null) {
            CassandraWriteConfig config = getCassandraConfig();
            NamingConventions.overrideVariableTableName(config.getTableName());
            try {
                File file = FileUtils.getFile(jsonFile);
                List<String> strings = FileUtils.readLines(file, "UTF-8");
                for (String line : strings) {
                    dataWriter.write(variable, line);
                }
            } finally {
                NamingConventions.stopOverrideVariableTableName();
            }
        }
    }


}
