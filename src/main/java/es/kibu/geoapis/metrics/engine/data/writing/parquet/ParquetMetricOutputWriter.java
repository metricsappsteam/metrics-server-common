package es.kibu.geoapis.metrics.engine.data.writing.parquet;

import es.kibu.geoapis.metrics.engine.data.conversion.AvroConverter;
import es.kibu.geoapis.metrics.engine.data.writing.GenericRecordWriter;
import es.kibu.geoapis.metrics.engine.data.writing.MetricOutputWriter;
import jdk.nashorn.api.scripting.ScriptObjectMirror;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericRecord;
import org.apache.commons.io.FileUtils;
//import org.apache.hadoop.fs.Path;
import org.apache.parquet.avro.AvroSchemaConverter;
import org.apache.parquet.avro.AvroWriteSupport;
import org.apache.parquet.hadoop.ParquetWriter;
import org.apache.parquet.hadoop.metadata.CompressionCodecName;
import org.apache.parquet.schema.MessageType;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by lrodr_000 on 14/10/2016.
 */
public class ParquetMetricOutputWriter implements MetricOutputWriter<ScriptObjectMirror>, GenericRecordWriter {
    @Override
    public void write(ScriptObjectMirror mirror) throws IOException {
        throw new RuntimeException("Not implemented after refactoring");
    }

    @Override
    public void writeRecords(List<GenericRecord> records) throws IOException {
        throw new RuntimeException("Not implemented after refactoring");
    }

    @Override
    public void write(List<ScriptObjectMirror> mirror) throws IOException {
        throw new RuntimeException("Not implemented after refactoring");
    }

    @Override
    public Object getOutputLocation() {
        //return null;
        throw new RuntimeException("Not implemented after refactoring");
    }
/* static Logger logger = LoggerFactory.getLogger(ParquetMetricOutputWriter.class);

    // set Parquet file block size and page size values
    int blockSize = 256 * 1024 * 1024;
    int pageSize = 64 * 1024;

    public String getOutputFilename() {
        return outputFilename;
    }

    String outputFilename = FileUtils.getUserDirectoryPath() + File.separator + "outputs" + File.separator +
            String.format("file_%s.parquet", DateTime.now().toDateTimeISO().toString().replace(":", ""));

    Path outputPath;
    // choose compression scheme
    CompressionCodecName compressionCodecName = CompressionCodecName.SNAPPY;


    MessageType parquetSchema;
    AvroWriteSupport writeSupport;
    AvroConverter converter =  new AvroConverter();


    public ParquetMetricOutputWriter(Schema avroSchema) {
        logger.debug("defaulting to output: {}", outputFilename);
        init(avroSchema, outputFilename);
    }

    public ParquetMetricOutputWriter(Schema avroSchema, String outputFilename) {
        this.outputFilename = outputFilename;
        init(avroSchema, outputFilename);
    }

    private  void init(Schema avroSchema, String outputFilename) {
        outputPath = new Path(outputFilename);

        parquetSchema = new AvroSchemaConverter().convert(avroSchema);

        // create a WriteSupport object to serialize your Avro objects
        writeSupport = new AvroWriteSupport(parquetSchema, avroSchema);

        converter.setAvroSchema(avroSchema);
    }

    @Override
    public void writeRecords(List<GenericRecord> records) throws IOException {

        logger.debug("Writing {} records.", records.size());
        // the ParquetWriter object that will consume Avro GenericRecords
        ParquetWriter parquetWriter = new
                ParquetWriter(outputPath,
                writeSupport, compressionCodecName, blockSize, pageSize);

        // Leave favorite color null
        for (GenericRecord record : records) {
            write(parquetWriter, record);
        }

        parquetWriter.close();
    }

    private void write (ParquetWriter writer, GenericRecord record) throws IOException {
        writer.write(record);
    }

    @Override
    public void write(ScriptObjectMirror mirror) throws IOException {
        ParquetWriter parquetWriter = new ParquetWriter(outputPath,  writeSupport, compressionCodecName, blockSize, pageSize);
        write(parquetWriter, mirror);
        parquetWriter.close();
    }


    private void write(ParquetWriter writer, ScriptObjectMirror mirror) throws IOException {
        GenericRecord genericRecord = converter.convert(mirror, "metricName", "application");
        writer.write(genericRecord);

    }

    @Override
    public void write(List<ScriptObjectMirror> objsMirror) throws IOException {
        ParquetWriter parquetWriter = new ParquetWriter(outputPath,  writeSupport, compressionCodecName, blockSize, pageSize);
        try {
            for (ScriptObjectMirror objectMirror : objsMirror) {
                write(parquetWriter, objectMirror);
            }
        } finally {
            parquetWriter.close();
        }
    }

    @Override
    public Object getOutputLocation() {
        return getOutputFilename();
    }*/
}
