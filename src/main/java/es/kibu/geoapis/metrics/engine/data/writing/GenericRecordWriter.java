package es.kibu.geoapis.metrics.engine.data.writing;

import org.apache.avro.generic.GenericRecord;

import java.io.IOException;
import java.util.List;

/**
 * Created by lrodr_000 on 25/10/2016.
 */
public interface GenericRecordWriter {

    void writeRecords(List<GenericRecord> records) throws IOException;
}
