package es.kibu.geoapis.metrics.engine.data.writing.cassandra;

import com.fasterxml.jackson.databind.JsonNode;
import es.kibu.geoapis.converters.SchemaToCassandraTableConverter;
import es.kibu.geoapis.metrics.engine.core.MetricsContextFields;
import es.kibu.geoapis.metrics.engine.data.conversion.VariableConverter;
import es.kibu.geoapis.metrics.engine.data.writing.NamingConventions;
import es.kibu.geoapis.objectmodel.MetricsDefinition;
import es.kibu.geoapis.objectmodel.Variable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static es.kibu.geoapis.objectmodel.dimensions.Constants.TIME;

/**
 * Created by lrodr_000 on 02/11/2016.
 */
public class CassandraVariableDataWriter extends BaseCassandraWriter {

    Variable variable;
    MetricsDefinition definition;

    private String getTableName(Variable variable, MetricsDefinition definition) {
        return NamingConventions.getVariableTableName(variable, definition);
    }

    public void prepareForVariable(Variable variable, MetricsDefinition definition) {
        this.variable = variable;
        this.definition = definition;

        JsonNode variableSchema = VariableConverter.getVariableSchema(variable);
        String tableName = getTableName(variable, definition);

        CassandraManager.CassandraManagerConfig config = getCassandraManagerConfig();

        Map<String, String> extraProperties = new HashMap<>();

        extraProperties.put(MetricsContextFields.TIME_ID, SchemaToCassandraTableConverter.CassandraTypes.TIMEUUID.getTypeName());

        config.setExtraProperties(extraProperties);

        createTableIfNotExists(tableName, variableSchema, config);

    }

    private CassandraManager.CassandraManagerConfig getCassandraManagerConfig() {
        List<String> fields = new ArrayList<>();
        fields.add(TIME);
        fields.add(MetricsContextFields.APPLICATION_ID);
        fields.add(MetricsContextFields.USER_ID);
        fields.add(MetricsContextFields.SESSION_ID);
        fields.add(MetricsContextFields.TIME_ID);

        CassandraManager.CassandraManagerConfig config = new CassandraManager.CassandraManagerConfig(CassandraManager.getInstance(getConfigurator()).getKeyspace());
        config.setPrimaryKeyFields(fields);
        return config;
    }


    public void write(Variable variable, String varDataJson) {
        String tableName = getTableName(variable, definition);
        String insertStatement = getCassandraManager().insertStatement(getKeyspace(),
                tableName, varDataJson);
        getCassandraManager().executeStatement(insertStatement);
    }

}
