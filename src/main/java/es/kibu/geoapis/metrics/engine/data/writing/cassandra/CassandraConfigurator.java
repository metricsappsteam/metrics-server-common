package es.kibu.geoapis.metrics.engine.data.writing.cassandra;

/**
 * Created by lrodr_000 on 24/02/2017.
 */
public interface CassandraConfigurator {
    CassandraSettings getSettings(Object settings);
}
