package es.kibu.geoapis.metrics.actors.data;

import akka.NotUsed;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.stream.javadsl.Flow;
import akka.stream.javadsl.Keep;
import akka.stream.javadsl.Sink;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.typesafe.config.Config;
import es.kibu.geoapis.metrics.actors.BaseKafkaConsumer;
import es.kibu.geoapis.metrics.actors.KafkaConsumerUtils;
import org.apache.kafka.clients.consumer.ConsumerRecord;

import java.io.IOException;

import static es.kibu.geoapis.metrics.actors.KafkaConsts.KAFKA_BOOTSTRAP_SERVERS;


/**
 * Created by lrodriguez2002cu on 02/04/2017.
 */
public class MetricsDefinitionCacheActorConsumer extends BaseKafkaConsumer<MetricsDefinitionCacheActor.MetricsDefinitionActorMessage> {

    public static final String TOPIC_METRICS_DEFINITION_CHANGE = "metrics-def-change";

    public MetricsDefinitionCacheActorConsumer() {
        super(TOPIC_METRICS_DEFINITION_CHANGE);
    }

    @Override
    protected MetricsDefinitionCacheActor.MetricsDefinitionActorMessage encode(ConsumerRecord<Object, Object> record) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            MetricsDefinitionCacheActor.MetricsDefinitionActorMessage msg
                    = mapper.readValue((String) record.value(), MetricsDefinitionCacheActor.MetricsDefinitionActorMessage.class);
            return msg;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static Sink<MetricsDefinitionCacheActor.MetricsDefinitionActorMessage, NotUsed> actorSink(ActorRef actorRef) {
        return Flow.of(MetricsDefinitionCacheActor.MetricsDefinitionActorMessage.class)
                .toMat(Sink.actorRef(actorRef, new KafkaConsumerUtils.CompleteObject()), Keep.none());
    }

    public static class SentToEventStream {

        static ActorSystem actorSystem;

        public static void sendToEventStream(MetricsDefinitionCacheActor.MetricsDefinitionActorMessage message) {
            if (actorSystem != null) {
                actorSystem.eventStream().publish(message);
            }
        }

        static void withActorSystem(ActorSystem system) {
            SentToEventStream.actorSystem = system;
        }

    }


    public static Sink<MetricsDefinitionCacheActor.MetricsDefinitionActorMessage, NotUsed> completeSink(ActorSystem system) {

        SentToEventStream.withActorSystem(system);
        /*final Sink<MetricsDefinitionCacheActor.MetricsDefinitionActorMessage, NotUsed> sink = Flow.of(MetricsDefinitionCacheActor.MetricsDefinitionActorMessage.class)
                .to(Sink.foreach(SentToEventStream::sendToEventStream));*/
        final Sink<MetricsDefinitionCacheActor.MetricsDefinitionActorMessage, NotUsed> sink = Flow.of(MetricsDefinitionCacheActor.MetricsDefinitionActorMessage.class)
                .to(Sink.foreach(SentToEventStream::sendToEventStream));

        return Flow.of(MetricsDefinitionCacheActor.MetricsDefinitionActorMessage.class)
                .toMat(sink, Keep.none());
    }

    public void start(ActorSystem actorSystem, Config config) {
        super.start(actorSystem, config, completeSink(actorSystem));
    }

    @Override
    protected String getBootstrapServersKey() {
        return KAFKA_BOOTSTRAP_SERVERS;
    }
}
