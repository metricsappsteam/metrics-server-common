package es.kibu.geoapis.metrics.actors.data.writer;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.pattern.Patterns;
import akka.util.Timeout;
import es.kibu.geoapis.metrics.actors.data.MetricsDefinitionCacheActor;
import es.kibu.geoapis.metrics.actors.data.WriteInstruction;
import es.kibu.geoapis.metrics.engine.data.writing.CassandraSimpleDataWriter;
import es.kibu.geoapis.metrics.engine.data.writing.CassandraWriteConfig;
import es.kibu.geoapis.metrics.engine.data.writing.WritingUtils;
import es.kibu.geoapis.metrics.engine.data.writing.cassandra.CassandraSettings;
import es.kibu.geoapis.metrics.engine.data.writing.cassandra.DefaultCassandraManagerConfigurator;
import es.kibu.geoapis.metrics.engine.data.writing.core.DataWriter;
import es.kibu.geoapis.objectmodel.MetricsDefinition;
import es.kibu.geoapis.objectmodel.Variable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.FiniteDuration;

import java.util.Optional;
import java.util.concurrent.TimeUnit;

/**
 * Created by lrodriguez2002cu on 28/12/2016.
 */
public class DataWriterActor extends UntypedActor {

    DataWriter writer;
    Logger logger = LoggerFactory.getLogger(DataWriterActor.class);
    boolean runFake = true;

    public static Props props(CassandraSettings settings) {
        return Props.create(DataWriterActor.class, settings);
    }

    public static Props props(CassandraSettings settings, boolean runFake) {
        return Props.create(DataWriterActor.class, settings, runFake);
    }

    @Override
    public void preStart() throws Exception {
        super.preStart();
        createWriter();
    }

    public DataWriterActor(CassandraSettings settings, boolean runFake) {
        this.settings = settings;
        this.runFake = runFake;
        if (runFake) {
            logger.debug("");
        }

    }

    public DataWriterActor(CassandraSettings settings) {
        this(settings, false);
    }

    @Override
    public void onReceive(Object message) throws Throwable {
        if (message instanceof WriteInstruction) {

            WriteInstruction writeInstruction = (WriteInstruction) message;

            if (!runFake) {
                if (writeInstruction.isResolved()) {
                    MetricsDefinition definition = writeInstruction.getDefinition();
                    doWrite(writeInstruction, definition);
                } else {
                    String application = writeInstruction.getContext().getApplication();
                    String metricId = writeInstruction.getMetricId();

                    Optional<MetricsDefinition> metricsDefinition = resolve(application, metricId);
                    if (metricsDefinition.isPresent()) {
                        MetricsDefinition definition = metricsDefinition.get();
                        writeInstruction.setDefinition(definition);
                        doWrite(writeInstruction, definition);
                    } else
                        logger.error("Data could not be saved  because the metrics definition  was not found app: {} metricId: {}", application, metricId);
                }
            } else {
                logger.debug("DataWriter received in fake mode: {}", message);
            }
        }
    }

    private void doWrite(WriteInstruction writeInstruction, MetricsDefinition definition) throws Exception {
        setupWriter(writeInstruction);
        writer.write(writeInstruction.getData(), definition);
    }

    public ActorRef getMetricsDefinitionActor() {
        //TODO: Check how to return resolve this so that actors are dynamically created and  used later
        return this.getContext().actorOf(MetricsDefinitionCacheActor.props());
    }

    public Optional<MetricsDefinition> resolve(String application, String metricsId) {

        //TODO: create a configuration with this.
        FiniteDuration atMost = FiniteDuration.create(10, TimeUnit.SECONDS);
        Future<Object> ask = Patterns.ask(getMetricsDefinitionActor(), new MetricsDefinitionCacheActor.MetricsDefinitionActorMessage(application, metricsId, MetricsDefinitionCacheActor.Action.GET),
                Timeout.apply(atMost));
        try {
            MetricsDefinitionCacheActor.MetricsDefinitionActorReplyMessage reply = (MetricsDefinitionCacheActor.MetricsDefinitionActorReplyMessage) Await.result(ask, atMost);
            if (!reply.isError()) {
                return Optional.of(reply.getMetricsDefinition());
            } else {
                return Optional.empty();
            }
        } catch (Exception e) {
            logger.error("Error while resolving metrics definition for  app: {},  metrics: {}", application, metricsId, e);
        }

        return Optional.empty();
    }

    private void createWriter() {
        writer = new CassandraSimpleDataWriter(new DefaultCassandraManagerConfigurator(settings));//new CassandraDataWriter(new DefaultCassandraManagerConfigurator(settings));
    }

    public void setupWriter(WriteInstruction instruction) {

        MetricsDefinition metricsDefinition = instruction.getDefinition();
        String variableName = instruction.getVariable();
        String tableName = WritingUtils.getVariableName(variableName, metricsDefinition);
        String keyspace = settings.getCassandraDBName();
        Variable variable = metricsDefinition.getVariableByName(variableName);

        writer.setConfig(new CassandraWriteConfig(keyspace, tableName, variable, metricsDefinition));
    }

    /*CassandraManager.getInstance()*/
    CassandraSettings settings;


}
