package es.kibu.geoapis.metrics.actors.data;

import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.cluster.pubsub.DistributedPubSubMediator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import es.kibu.geoapis.metrics.MetricUtils;
import es.kibu.geoapis.metrics.actors.ActorLogger;
import es.kibu.geoapis.objectmodel.MetricsDefinition;
import es.kibu.geoapis.services.MetricsManager;
import es.kibu.geoapis.services.MetricsStorageUtils;
import org.ehcache.Cache;
import org.ehcache.CacheManager;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.CacheManagerBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import scala.reflect.ClassTag;

import java.io.IOException;
import java.io.Serializable;
import java.util.Optional;

/**
 * Created by lrodriguez2002cu on 28/12/2016.
 */
public class MetricsDefinitionCacheActor extends UntypedActor {

    public static final String METRICS_DEFINITION_CACHE = "metricsDefinitionCache";
    //public static final String METRICS_DEF_CHANGED = "metrics-def-changed";

    //Logger logger = LoggerFactory.getLogger(MetricsDefinitionCacheActor.class);
    ActorLogger logger = new ActorLogger(this);//LoggerFactory.getLogger(MetricsDefinitionCacheActor.class);

    CacheManager cacheManager;
    Cache<String, String> metricsDefinitionCache;

    public static Props props() {
        ClassTag<MetricsDefinitionCacheActor> tag = scala.reflect.ClassTag$.MODULE$.apply(MetricsDefinitionCacheActor.class);
        return Props.apply(tag);
    }

    @Override
    public void preStart() throws Exception {
        super.preStart();
        initCache();

        /*ActorRef mediator = DistributedPubSub.get(getContext().system()).mediator();
        // subscribe to the topic named "content"
        mediator.tell(new DistributedPubSubMediator.Subscribe(METRICS_DEF_CHANGED, getSelf()),
                getSelf());
        */
        logger.info("Subscribing to the event stream..");
        getContext().system().eventStream().subscribe(getSelf(), MetricsDefinitionActorMessage.class);
    }

    private void initCache() {
        logger.info("Initializing cache..");
        cacheManager = CacheManagerBuilder.newCacheManagerBuilder()
                .withCache(METRICS_DEFINITION_CACHE,
                        CacheConfigurationBuilder.newCacheConfigurationBuilder(String.class, String.class,
                                ResourcePoolsBuilder.heap(100))
                                .build())
                .build(true);

        metricsDefinitionCache
                = cacheManager.getCache(METRICS_DEFINITION_CACHE, String.class, String.class);

    }

    private boolean isInCache(String application, String metricId) {
        String key = getKey(application, metricId);

        return isInCache(key);
    }

    private boolean isInCache(String key) {
        boolean foundInCache = metricsDefinitionCache.containsKey(key);
        logger.debug("Checking if in cache: {} ({})", key, foundInCache);
        return foundInCache;
    }


    private void invalidate(String application, String metricId) {
        invalidate(getKey(application, metricId));
        invalidate(getKey(application, null));
    }

    private void invalidate(String key) {
        logger.info("Invalidating cache for key: {}", key);
        if (metricsDefinitionCache.containsKey(key)) {
            metricsDefinitionCache.remove(key);
        }
    }

    private void invalidateAll() {
        logger.info("Invalidating the full cache");
        metricsDefinitionCache.clear();
    }

    public static class MetricNotFoundException extends RuntimeException {
        String application;
        String metricsId;

        public MetricNotFoundException(String message, String application, String metricsId) {
            super(message);
            this.application = application;
            this.metricsId = metricsId;
        }

        public String getApplication() {
            return application;
        }

        public void setApplication(String application) {
            this.application = application;
        }

        public String getMetricsId() {
            return metricsId;
        }

        public void setMetricsId(String metricsId) {
            this.metricsId = metricsId;
        }
    }

    private MetricsDefinition getMetricDefinition(String application, String metricsId) throws IOException, ProcessingException {
        if (isInCache(application, metricsId)) {
            logger.debug("Retrieving from cache");
            String metricsDefinition = metricsDefinitionCache.get(getKey(application, metricsId));
            return MetricUtils.withMetricDefinitionFromContent(metricsDefinition);
        } else {
            Optional<String> fromStorage = getFromStorage(application);
            if (fromStorage.isPresent()) {
                logger.debug("Retrieving from database");
                String metricDefinitionStr = fromStorage.get();
                metricsDefinitionCache.put(getKey(application, metricsId), metricDefinitionStr);
                MetricsDefinition metricsDefinition = MetricUtils.withMetricDefinitionFromContent(metricDefinitionStr);
                return metricsDefinition;
            } else throw new MetricNotFoundException("Metric was not found", application, metricsId);
        }
    }

    private String getKey(String application, String metricsId) {
        String key = CacheKey.of(application, metricsId);
        logger.debug("Accessing key: {}", key);
        return key;
    }

    MetricsManager metricsManager;

    public Optional<MetricsStorageUtils.MetricsDef> getMetricsDef(String applicationId) {
        if (metricsManager == null) {
            logger.debug("Creating the manager");
            metricsManager = MetricsStorageUtils.getMetricsManager(this.context().system());
        }
        return metricsManager.getMetricsDef(applicationId);
    }

    private Optional<String> getFromStorage(String application) {
        Optional<MetricsStorageUtils.MetricsDef> metricsDef = getMetricsDef(application);
        if (metricsDef.isPresent()) {
            return Optional.of(metricsDef.get().getContent());
        } else return Optional.empty();
    }

    public static class CacheKey {
        public static String of(String application, String metricsDefinitionId) {
            /*if (metricsDefinitionId != null) {
                return String.format("%s:%s", application, metricsDefinitionId);
            } else */return String.format("%s", application);
        }
    }

    public enum Action {INVALIDATE, GET, INVALIDATE_ALL}

    public static class MetricsDefinitionActorMessage implements Serializable {

        String application;
        String metricId;

        Action action = Action.GET;

        public Action getAction() {
            return action;
        }

        public void setAction(Action action) {
            this.action = action;
        }

        //used only for serialization purposes
        public MetricsDefinitionActorMessage() {
        }

        public MetricsDefinitionActorMessage(String application, String metricId) {
            this(application, metricId, Action.GET);
        }

        public MetricsDefinitionActorMessage(String application, String metricId, Action action) {
            this.application = application;
            this.metricId = metricId;
            this.action = action;
        }

        public static MetricsDefinitionActorMessage createGetMessage(String application, String metricId) {
            return new MetricsDefinitionActorMessage(application, metricId, Action.GET);
        }

        public static MetricsDefinitionActorMessage createGetMessage(String application) {
            return new MetricsDefinitionActorMessage(application, null, Action.GET);
        }

        public static MetricsDefinitionActorMessage createInvalidateMessage(String application, String metricId) {
            return new MetricsDefinitionActorMessage(application, metricId, Action.INVALIDATE);
        }

        public static MetricsDefinitionActorMessage createInvalidateAllMessage() {
            return new MetricsDefinitionActorMessage(null, null, Action.INVALIDATE_ALL);
        }

        public String getApplication() {
            return application;
        }

        public void setApplication(String application) {
            this.application = application;
        }

        public String getMetricId() {
            return metricId;
        }

        public void setMetricId(String metricId) {
            this.metricId = metricId;
        }

        @Override
        public String toString() {
            return "MetricsDefinitionActorMessage{" +
                    "application='" + application + '\'' +
                    ", metricRef='" + metricId + '\'' +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            MetricsDefinitionActorMessage that = (MetricsDefinitionActorMessage) o;

            if (getApplication() != null ? !getApplication().equals(that.getApplication()) : that.getApplication() != null)
                return false;
            if (getMetricId() != null ? !getMetricId().equals(that.getMetricId()) : that.getMetricId() != null)
                return false;
            return getAction() == that.getAction();
        }

        @Override
        public int hashCode() {
            int result = getApplication() != null ? getApplication().hashCode() : 0;
            result = 31 * result + (getMetricId() != null ? getMetricId().hashCode() : 0);
            result = 31 * result + (getAction() != null ? getAction().hashCode() : 0);
            return result;
        }

        public String toJson() throws JsonProcessingException {
            ObjectMapper mapper = new ObjectMapper();
            MetricsDefinitionCacheActor.MetricsDefinitionActorMessage getMessage
                    = MetricsDefinitionCacheActor.MetricsDefinitionActorMessage.createGetMessage("application", "metricId");
            String serialized = mapper.writeValueAsString(getMessage);
            return serialized;
        }

        public static MetricsDefinitionActorMessage fromJson(String json) throws IOException {
            ObjectMapper mapper = new ObjectMapper();
            MetricsDefinitionCacheActor.MetricsDefinitionActorMessage msg =
                    mapper.readValue(json, MetricsDefinitionCacheActor.MetricsDefinitionActorMessage.class);
            return  msg;
        }
    }

    public static class MetricsDefinitionActorReplyMessage implements Serializable {

        MetricsDefinition metricsDefinition;

        public MetricsDefinition getMetricsDefinition() {
            return metricsDefinition;
        }

        String application;
        String metricId;
        Exception ex;

        public MetricsDefinitionActorReplyMessage(MetricsDefinition metricsDefinition, String application, String metricId) {
            this.metricsDefinition = metricsDefinition;
            this.application = application;
            this.metricId = metricId;
        }

        public MetricsDefinitionActorReplyMessage(String application, String metricId, Exception ex) {
            this.application = application;
            this.metricId = metricId;
            this.ex = ex;
        }

        public static MetricsDefinitionActorReplyMessage from(MetricsDefinitionActorMessage requestMessage, MetricsDefinition definition) {
            return new MetricsDefinitionActorReplyMessage(definition, requestMessage.application, requestMessage.metricId);
        }

        public static MetricsDefinitionActorReplyMessage from(MetricsDefinitionActorMessage requestMessage, Exception ex) {
            return new MetricsDefinitionActorReplyMessage(requestMessage.application, requestMessage.metricId, ex);
        }

        public boolean isError() {
            return ex != null;
        }

        public Exception getError() {
            return ex;
        }

        @Override
        public String toString() {
            return "MetricsDefinitionActorReplyMessage{" +
                    "metricsDefinition=" + metricsDefinition +
                    ", application='" + application + '\'' +
                    ", metricRef='" + metricId + '\'' +
                    ", ex=" + ((ex != null) ? ex.getMessage() : "") +
                    ", isError=" + isError() +
                    '}';
        }
    }


    @Override
    public void onReceive(Object message) throws Throwable {

        if (message instanceof MetricsDefinitionActorMessage) {
            MetricsDefinitionActorMessage msg = (MetricsDefinitionActorMessage) message;
            MetricsDefinitionActorReplyMessage resultMessage = null;

            String application = msg.application;
            String metricId = msg.metricId;

            switch (((MetricsDefinitionActorMessage) message).action) {

                case INVALIDATE:
                    logger.debug("Invalidating metrics definitions for app: {}, metric: {}", application, metricId);
                    invalidate(application, metricId);
                    break;

                case INVALIDATE_ALL:
                    logger.debug("Invalidating all the metrics definitions.");
                    invalidateAll();
                    break;

                case GET: {
                    logger.info("Retrieving Metrics Definition  app: {}, metric: {}", application, metricId);
                    try {
                        MetricsDefinition metricDefinition = getMetricDefinition(application, metricId);
                        resultMessage = MetricsDefinitionActorReplyMessage.from(msg, metricDefinition);
                        logger.info("Retrieved Metrics Definition  app: {}, metric: {}", application, metricId);
                    } catch (Exception e) {
                        resultMessage = MetricsDefinitionActorReplyMessage.from(msg, e);
                        logger.error("Metrics Definition could not be retrieved app: {}, metric: {}, error: {}", application, metricId, e.getMessage());
                    }
                    getSender().tell(resultMessage, getSelf());
                }
                break;
            }

        }/* else if (message instanceof DistributedPubSubMediator.SubscribeAck) {
            logger.info("Subscribed to: {}", METRICS_DEF_CHANGED);
        }*/ else unhandled(message);
    }


}
