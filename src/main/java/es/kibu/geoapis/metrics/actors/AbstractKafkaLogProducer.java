package es.kibu.geoapis.metrics.actors;

import akka.Done;
import akka.actor.ActorRef;
import akka.actor.UntypedActor;
import akka.event.Logging;
import akka.kafka.ProducerSettings;
import akka.kafka.javadsl.Producer;
import akka.stream.ActorMaterializer;
import akka.stream.Materializer;
import akka.stream.OverflowStrategy;
import akka.stream.QueueOfferResult;
import akka.stream.javadsl.RunnableGraph;
import akka.stream.javadsl.Sink;
import akka.stream.javadsl.Source;
import akka.stream.javadsl.SourceQueueWithComplete;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.concurrent.CompletionStage;

import static es.kibu.geoapis.metrics.actors.ActorLogger.MSG_ID_MDC_FIELD;
import static es.kibu.geoapis.metrics.actors.ActorLogger.SUB_TOPIC;

/**
 * Created by lrodriguez2002cu on 02/04/2017.
 */
public abstract class AbstractKafkaLogProducer extends UntypedActor {

    public static final String GENERAL = "general";
    public static final String METRICS_EXEC_LOG = "metrics_exec_log";

    final Materializer materializer = ActorMaterializer.create(this.context());

    protected ProducerSettings<String, String> producerSettings;
    Logger logger = LoggerFactory.getLogger(AbstractKafkaLogProducer.class);

    ActorRef testProbe;

    SourceQueueWithComplete<Object> queue;
    private Sink<ProducerRecord<String, String>, CompletionStage<Done>> kafkaSink;

    @Override
    public void preStart() throws Exception {
        super.preStart();
        getProducerSettings();
    }

    private String getLogTopic(String id) {
        return "log-topic-" + id;
    }

    @Override
    public void onReceive(Object message) throws Throwable {

        LogData logData = getLogData(message);
        if (logData != null) {
            logger.debug("Message received  by kafka logger: {}", message);
            getQueue().offer(logData)
                    .handleAsync((offerResult, throwable) -> {
                        boolean failed = (offerResult == null) || !offerResult.equals(QueueOfferResult.enqueued());
                        failed = failed || offerResult.equals(QueueOfferResult.dropped());

                        if (throwable != null || failed) {
                            if (throwable != null) {
                                logger.error("Error while sending kafka producer record:", throwable);
                            } else {
                                String msg = "Error while sending kafka producer record: data is not enqeued into the stream";
                                logger.error(msg);
                            }

                            notifyTestActorRef(MSG_STATUS.FAILURE);

                        } else {
                            logger.info("Data enqeued into the stream.");
                            notifyTestActorRef(MSG_STATUS.SENT);
                        }
                        return true;
                    });
        }
        else {
            if (message instanceof ActorRef) {
                //this might be the case in tests ...
                logger.debug("Setting up probe actor ref {}", message);
                testProbe = (ActorRef) message;
            }
        }

    }

    enum MSG_STATUS { FAILURE, SENT };

    private void  notifyTestActorRef(MSG_STATUS status){
        if (testProbe != null) {
            //testProbe.tell(String.format("status: %s, message: %s", status.name(), message), self());
            testProbe.tell(String.format("status: %s", status.name()), self());
        }
    }

    private LogData getLogData(Object message) {

        LogData logData = null;

        if (message instanceof LogData) {
            //getDoneCompletionStage(materializer, (LogData)message);
            logData = (LogData) message;
        } else if (message instanceof String) {
            //getSubmitCompletionStage(materializer, (String) message, GENERAL);
            logData = new LogData(GENERAL, (String) message);
        } else if (message instanceof Logging.LogEvent) {
            Logging.LogEvent msg = (Logging.LogEvent) message;
            Object content = msg.message();
            //String logSource = ((Logging.LogEvent) message).logSource();
            String topic = GENERAL;
            if (msg.getMDC().containsKey(MSG_ID_MDC_FIELD)) {
                String msgId = (String) msg.getMDC().get(MSG_ID_MDC_FIELD);
                topic = getLogTopic(msgId);
            }
            if (msg.getMDC().containsKey(SUB_TOPIC)) {
                String msgId = (String) msg.getMDC().get(SUB_TOPIC);
                topic = getLogTopic(msgId);
            }
            //getSubmitCompletionStage(materializer, (String) content, topic);
            logData = new LogData(topic, (String) content);
        } else {


        }

        return logData;
    }

    protected abstract ProducerSettings getProducerSettings();

    public class LogData implements Serializable {

        private String channel;
        private String content;

        public LogData(String channel, String content) {
            this.channel = channel;
            this.content = content;
        }

        public LogData(String content) {
            this.content = content;
            this.channel = GENERAL;
        }

        public String getChannel() {
            return channel;
        }

        public String getContent() {
            return content;
        }
    }

    private Sink<ProducerRecord<String, String>, CompletionStage<Done>> createKafkaSink(boolean fake) {
        boolean artificial = fake || false;
        if (kafkaSink == null || artificial) {
            if (artificial) {
                kafkaSink = Sink.foreach(param -> {
                    throw new RuntimeException(String.format("Failed while receiving key: %s value: %s", param.key(), param.value()));
                    //Logger.debug(String.format("Sink received key: %s value: %s", param.key(), param.value()));
                });
            } else kafkaSink = Producer.plainSink(getProducerSettings());
        }
        return kafkaSink;
    }

    private RunnableGraph<ActorRef> runnableGraphToKafkaActor() {
        //new VariableData(topicName, content)
        RunnableGraph<ActorRef> to = Source.actorRef(100, OverflowStrategy.fail())
                .map(rawElem -> {
                    LogData elem = (LogData) rawElem;
                    //Logger.debug("Sending data to topic: {}, key: {}", METRICS_EXEC_LOG, elem.getChannel());
                    return new ProducerRecord<>(METRICS_EXEC_LOG, elem.getChannel(), elem.getContent());
                })
                .to(createKafkaSink());
        return to; //.runWith(createKafkaSink(), materializer);
    }

    private RunnableGraph<SourceQueueWithComplete<Object>> runnableGraphToKafkaQueue() {
        //new VariableData(topicName, content)
        RunnableGraph<SourceQueueWithComplete<Object>> to = Source.queue(0, OverflowStrategy.dropTail())
                .map(rawElem -> {
                    LogData elem = (LogData) rawElem;
                    //Logger.debug("Sending data to topic: {}, key: {}", METRICS_EXEC_LOG, elem.getChannel());
                    return new ProducerRecord<>(METRICS_EXEC_LOG, elem.getChannel(), elem.getContent());
                })
                .to(createKafkaSink());
        return to; //.runWith(createKafkaSink(), materializer);
    }

    private SourceQueueWithComplete<Object> getQueue() {
        return getQueue(materializer);
    }

    private SourceQueueWithComplete<Object> getQueue(Materializer materializer) {
        if (queue == null) {
            queue = createQueueSourceGraph(materializer);
        }
        return queue;
    }

    private SourceQueueWithComplete<Object> createQueueSourceGraph(Materializer materializer) {
        RunnableGraph<SourceQueueWithComplete<Object>> sourceQueueWithCompleteRunnableGraph = runnableGraphToKafkaQueue();
        SourceQueueWithComplete<Object> run = sourceQueueWithCompleteRunnableGraph.run(materializer);
        return run;
    }

    private Sink<ProducerRecord<String, String>, CompletionStage<Done>> createKafkaSink() {
        return createKafkaSink(false);
    }

}
