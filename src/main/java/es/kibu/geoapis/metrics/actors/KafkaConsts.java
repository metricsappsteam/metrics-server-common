package es.kibu.geoapis.metrics.actors;

/**
 * Created by lrodr_000 on 03/04/2017.
 */
public class KafkaConsts {
    public static final String KAFKA_BOOTSTRAP_SERVERS = "kafka-bootstrap-servers";

    public static final String METRICS_DATA = "metrics_data";
    public static final String DATA_CHANGE = "data_change";

}
