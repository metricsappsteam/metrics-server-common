package es.kibu.geoapis.metrics.actors.messages;

import es.kibu.geoapis.backend.actors.messages.IdentifiableReplyMessage;
import es.kibu.geoapis.metrics.engine.results.MetricEvaluationResult;
import es.kibu.geoapis.services.objectmodel.results.MetricsResult;

import java.io.Serializable;

/**
 * Created by lrodriguez2002cu on 29/09/2016.
 */
public class MetricsResultMessage extends IdentifiableReplyMessage {

    MetricsMessage requestMessage;

    MetricEvaluationResult result;

    String error;

    String errorDetails;

    boolean failed;

    public boolean isFailed() {
        return failed;
    }

    public String getErrorDetails() {
        return errorDetails;
    }

    public void setErrorDetails(String errorDetails) {
        this.errorDetails = errorDetails;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public void setFailed(boolean failed) {
        this.failed = failed;
    }

    public MetricsResultMessage(MetricsMessage requestMessage, MetricEvaluationResult result) {
        super(requestMessage);
        this.requestMessage = requestMessage;
        this.result = result;
    }

    public MetricsResultMessage(MetricsMessage requestMessage, String error, String errorDetails) {
        super(requestMessage);
        this.requestMessage = requestMessage;
        this.result = null;
        this.error = error;
        this.errorDetails = errorDetails;

    }

    public MetricsMessage getRequestMessage() {
        return requestMessage;
    }

    public void setRequestMessage(MetricsMessage requestMessage) {
        this.requestMessage = requestMessage;
    }

    public MetricEvaluationResult getResult() {
        return result;
    }

    public void setResult(MetricEvaluationResult result) {
        this.result = result;
    }

    public static MetricsResultMessage forError(MetricsMessage requestMessage, String error, String errorDetails) {
          return  new MetricsResultMessage(requestMessage, error, errorDetails);
    }

    @Override
    public String toString() {
        return "MetricsResultMessage{" +
                "requestMessage=" + requestMessage +
                ", result=" + result +
                '}';
    }
}
