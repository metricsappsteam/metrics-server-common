package es.kibu.geoapis.metrics.actors;

import akka.actor.ActorSystem;
import akka.actor.UntypedActor;
import akka.event.DiagnosticLoggingAdapter;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import es.kibu.geoapis.backend.actors.messages.IdentifiableMessage;
import org.slf4j.Logger;
import org.slf4j.Marker;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by lrodr_000 on 27/03/2017.
 */
public class ActorLogger implements Logger {

    public static final String MSG_ID_MDC_FIELD = "msg-id";
    public static final String SUB_TOPIC = "subtopic";

    //Logger logger = LoggerFactory.getLogger(ScriptEngineBackend.class);
    final LoggingAdapter logger;

    public ActorLogger(UntypedActor actor) {
        logger = Logging.getLogger(actor);
    }

    public ActorLogger(ActorSystem actorSystem, String logSource) {
        logger = Logging.getLogger(actorSystem, logSource);
    }


    public void endLoggerForMessage(IdentifiableMessage metricsMessage) {
        endLoggerMDC();
    }

    private void endLoggerMDC() {
        if (logger instanceof  DiagnosticLoggingAdapter) ((DiagnosticLoggingAdapter)logger).clearMDC();
    }

    public void prepareLoggerForMessage(IdentifiableMessage metricsMessage) {
        if (logger instanceof DiagnosticLoggingAdapter) {
            Map<String, Object> mdc = new HashMap<>();
            endLoggerForMessage(metricsMessage);
            mdc.put(MSG_ID_MDC_FIELD, metricsMessage.getId());
            ((DiagnosticLoggingAdapter)logger).setMDC(mdc);
        }

    }

    public void prepareLoggerSubtopic(String subtopic) {
        if (logger instanceof DiagnosticLoggingAdapter) {
            Map<String, Object> mdc = new HashMap<>();
            endLoggerMDC();
            mdc.put(SUB_TOPIC, subtopic);
            ((DiagnosticLoggingAdapter)logger).setMDC(mdc);
        }
    }

    public void debug(String message) {
        logger.debug(message);
    }

    public void info(String message) {
        logger.info(message);
    }

    public void error(String message, Object param) {
        logger.error(message, param);
    }

    public void error(String template, Object param, Object param1, Object param2) {
        logger.error(template, param, param1, param2);
    }

    public void error(String message, Object arg1, Object arg2) {
        logger.error(message, arg1, arg2);
    }

    public void debug(String message, Object arg1, Object arg2) {
        logger.debug(message, arg1, arg2);
    }

    public void debug(String message, Object arg1) {
        logger.debug(message, arg1);
    }

    public void info(String message, Object arg1) {
        logger.info(message, arg1);
    }

    public void info(String message, Object arg1, Object arg2) {
        logger.info(message, arg1, arg2);
    }


    @Override
    public String getName() {
        return "ActorLogger";
    }

    @Override
    public boolean isTraceEnabled() {
        return logger.isDebugEnabled();
    }

    @Override
    public void trace(String s) {
        logger.debug(s);
    }

    @Override
    public void trace(String s, Object o) {
        logger.debug(s, o);

    }

    @Override
    public void trace(String s, Object o, Object o1) {
        logger.debug(s, o, o1);

    }

    @Override
    public void trace(String s, Object... objects) {
        logger.debug(s, objects);
    }

    @Override
    public void trace(String s, Throwable throwable) {
        logger.debug(s, throwable);
    }

    @Override
    public boolean isTraceEnabled(Marker marker) {
        return false;
    }

    @Override
    public void trace(Marker marker, String s) {
        logger.debug("marker: " + s);
    }

    @Override
    public void trace(Marker marker, String s, Object o) {
        logger.debug("marker: " + s, o);
    }

    @Override
    public void trace(Marker marker, String s, Object o, Object o1) {
        logger.debug("marker: " + s, o, o1);
    }

    @Override
    public void trace(Marker marker, String s, Object... objects) {
        logger.debug("marker: " + s, objects);
    }

    @Override
    public void trace(Marker marker, String s, Throwable throwable) {
        logger.debug("marker: " + s, throwable);
    }

    @Override
    public boolean isDebugEnabled() {
        return logger.isDebugEnabled();
    }

    @Override
    public void debug(String s, Object... objects) {
        logger.debug("marker: " + s, objects);
    }

    @Override
    public void debug(String s, Throwable throwable) {
        logger.debug("marker: " + s, throwable);
    }

    @Override
    public boolean isDebugEnabled(Marker marker) {
        return logger.isDebugEnabled();
    }

    @Override
    public void debug(Marker marker, String s) {
        logger.debug("marker: " + s);
    }

    @Override
    public void debug(Marker marker, String s, Object o) {
        logger.debug("marker: " + s, o);
    }

    @Override
    public void debug(Marker marker, String s, Object o, Object o1) {
        logger.debug("marker: " + s, o, o1);
    }

    @Override
    public void debug(Marker marker, String s, Object... objects) {
        logger.debug("marker: " + s, objects);
    }

    @Override
    public void debug(Marker marker, String s, Throwable throwable) {
        logger.debug("marker: " + s, throwable);
    }

    @Override
    public boolean isInfoEnabled() {
        return logger.isInfoEnabled();
    }

    @Override
    public void info(String s, Object... objects) {
        logger.info("marker: " + s, objects);
    }

    @Override
    public void info(String s, Throwable throwable) {
        logger.info("marker: " + s, throwable);
    }

    @Override
    public boolean isInfoEnabled(Marker marker) {
        return logger.isInfoEnabled();
    }

    @Override
    public void info(Marker marker, String s) {
        logger.info("marker: " + s);
    }

    @Override
    public void info(Marker marker, String s, Object o) {
        logger.info("marker: " + s, o);
    }

    @Override
    public void info(Marker marker, String s, Object o, Object o1) {
        logger.info("marker: " + s, o, o1);
    }

    @Override
    public void info(Marker marker, String s, Object... objects) {
        logger.info("marker: " + s, objects);
    }

    @Override
    public void info(Marker marker, String s, Throwable throwable) {
        logger.info("marker: " + s, throwable);
    }

    @Override
    public boolean isWarnEnabled() {
        return logger.isWarningEnabled();
    }

    @Override
    public void warn(String s) {
        logger.warning("marker: " + s);
    }

    @Override
    public void warn(String s, Object o) {
        logger.warning("marker: " + s, o);
    }

    @Override
    public void warn(String s, Object... objects) {
        logger.warning(s, objects);
    }

    @Override
    public void warn(String s, Object o, Object o1) {
        logger.warning(s, o, o1);
    }

    @Override
    public void warn(String s, Throwable throwable) {
        logger.warning(s, throwable);
    }

    @Override
    public boolean isWarnEnabled(Marker marker) {
        return logger.isWarningEnabled();
    }

    @Override
    public void warn(Marker marker, String s) {
        logger.warning(s);
    }

    @Override
    public void warn(Marker marker, String s, Object o) {
        logger.warning("marker:" + s, o);
    }

    @Override
    public void warn(Marker marker, String s, Object o, Object o1) {
        logger.warning("marker:" + s, o, o1);
    }

    @Override
    public void warn(Marker marker, String s, Object... objects) {
        logger.warning("marker:" + s, objects);
    }

    @Override
    public void warn(Marker marker, String s, Throwable throwable) {
        logger.warning("marker:" + s, throwable);
    }

    @Override
    public boolean isErrorEnabled() {
        return logger.isErrorEnabled();
    }

    @Override
    public void error(String s) {
        logger.error(s);
    }

    @Override
    public void error(String s, Object... objects) {
        logger.error(s, objects);
    }

    @Override
    public void error(String s, Throwable throwable) {
        logger.error(s, throwable);
    }

    @Override
    public boolean isErrorEnabled(Marker marker) {
        return logger.isErrorEnabled();
    }

    @Override
    public void error(Marker marker, String s) {
        logger.error("marker: " + s);
    }

    @Override
    public void error(Marker marker, String s, Object o) {
        logger.error("marker: " + s, o);
    }

    @Override
    public void error(Marker marker, String s, Object o, Object o1) {
        logger.error("marker: " + s, o, o1);
    }

    @Override
    public void error(Marker marker, String s, Object... objects) {
        logger.error("marker: " + s, objects);
    }

    @Override
    public void error(Marker marker, String s, Throwable throwable) {
        logger.error("marker: " + s, throwable);
    }
}
