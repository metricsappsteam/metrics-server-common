package es.kibu.geoapis.metrics.actors.messages;

import es.kibu.geoapis.backend.actors.messages.IdentifiableMessage;
import es.kibu.geoapis.metrics.engine.core.BaseMetricsContext;
import es.kibu.geoapis.objectmodel.MetricsDefinition;

import java.io.Serializable;

/**
 * Created by lrodriguez2002cu on 22/08/2016.
 */
public class MetricsMessage extends IdentifiableMessage {


    public enum EvaluationMode {
        /**
         * Indicates to the engine that only the metrics depending (at all levels) on the specified variable should
         * be evaluated
         */
        evImmediateOnly,
        /**
         * Instruct the engine to evaluate the specified metric
         */
        evMetric,

        /**
         * Indicates to the engine that only the metrics depending (directly) on the specified variable should
         * be evaluated.
         */
        evAllChanged,

        /**
         * Indicates to the engine that only the metrics depending (directly) on the specified variable should
         * be evaluated.
         */
        evRender
    };

    EvaluationMode evaluationMode;

    public EvaluationMode getEvaluationMode() {
        return evaluationMode;
    }

    public void setEvaluationMode(EvaluationMode evaluationMode) {
        this.evaluationMode = evaluationMode;
    }

    MetricsDefinition metricsDefinition;

    public String getMetricsDefinitionId() {
        return metricsDefinitionId;
    }

    public void setMetricsDefinitionId(String metricsDefinitionId) {
        this.metricsDefinitionId = metricsDefinitionId;
    }

    String metricsDefinitionId;

    private String metric;

    private String variable;

    public String getVariable() {
        return variable;
    }

    private BaseMetricsContext context;

    private String dataFilesDir;

    public String getDataFilesDir() {
        return dataFilesDir;
    }

    public void setDataFilesDir(String dataBaseDir) {
        this.dataFilesDir = dataBaseDir;
    }

    public BaseMetricsContext getContext() {
        return context;
    }

    public void setContext(BaseMetricsContext context) {
        this.context = context;
    }

    public String getMetric() {
        return metric;
    }

    public void setMetric(String metric) {
        this.metric = metric;
    }

    protected MetricsMessage(MetricsDefinition metricsDefinition, String metric, BaseMetricsContext context, String dataFilesDir) {
        this.metricsDefinition = metricsDefinition;
        this.metric = metric;
        this.context = context;
        this.setDataFilesDir(dataFilesDir);
        this.evaluationMode = EvaluationMode.evMetric;
        this.engineConfig =  new Config(true, true);

    }

    protected MetricsMessage(MetricsDefinition metricsDefinition, String variableName, boolean immediateOnly, BaseMetricsContext context, String dataFilesDir) {
        this.metricsDefinition = metricsDefinition;
        this.variable = variableName;
        this.context = context;
        //TODO: check if this parameter is still necessary.
        this.setDataFilesDir(dataFilesDir);
        this.evaluationMode = (immediateOnly) ? EvaluationMode.evImmediateOnly : EvaluationMode.evAllChanged;
        this.engineConfig =  new Config(true, true);
    }

    /**
     * Creates a MetricsMessage for rendering
     * @param metricsDefinition the metrics definition
     * @param context the context
     * @param dataFilesDir a dataFiles dir (leave empty)
     */
    protected MetricsMessage(MetricsDefinition metricsDefinition, BaseMetricsContext context, String dataFilesDir) {
        this.metricsDefinition = metricsDefinition;
        this.metric = "";
        this.context = context;
        //TODO: check if this parameter is still necessary.
        this.setDataFilesDir(dataFilesDir);
        this.evaluationMode = EvaluationMode.evRender;
        this.engineConfig =  new Config(false, false);

    }


    public MetricsDefinition getMetricsDefinition() {
        return metricsDefinition;
    }

    public void setMetricsDefinition(MetricsDefinition metricsDefinition) {
        this.metricsDefinition = metricsDefinition;
    }

    public static MetricsMessage forMetric(MetricsDefinition metricsDefinition, String metric, BaseMetricsContext context, String dataFilesDir) {
        return new MetricsMessage(metricsDefinition, metric, context, dataFilesDir);
    }

    public static MetricsMessage forMetric(String metric, BaseMetricsContext context) {
        return MetricsMessage.forMetric(null, metric, context, "");
    }

    public static MetricsMessage forRender(MetricsDefinition metricsDefinition, BaseMetricsContext context, String dataFilesDir) {
        return new MetricsMessage(metricsDefinition,  context, dataFilesDir);
    }

    public static MetricsMessage forRender(BaseMetricsContext context) {
        return MetricsMessage.forRender(null,  context, "");
    }

    public static MetricsMessage forVariable(MetricsDefinition metricsDefinition, String variable, boolean inmediateOnly, BaseMetricsContext context, String dataFilesDir) {
        return new MetricsMessage(metricsDefinition, variable, inmediateOnly, context, dataFilesDir);
    }

    /**
     * Creates an unresolved message as the metrics definition need to be solved larer
     * @param variable the variable
     * @param inmediateOnly the policy.. only immediate or all dependant metrics
     * @param context the context
     * @return a newly created unresolved metric message.
     */
    public static MetricsMessage forVariable(String variable, boolean inmediateOnly, BaseMetricsContext context) {
        return new MetricsMessage(null, variable, inmediateOnly, context, "");
    }


    public boolean isResolved() {
        return metricsDefinition != null;
    }

    public String getTarget() {

        if (metric != null) {
            return metric;
        } else if (variable != null) {
            return variable;
        } else {
            return "<render>";
        }

    }


    public static class Config implements Serializable {

        boolean saveData;

        public boolean isEnableActionsNotifications() {
            return enableActionsNotifications;
        }

        public void setEnableActionsNotifications(boolean enableActionsNotifications) {
            this.enableActionsNotifications = enableActionsNotifications;
        }

        boolean enableActionsNotifications = true;

        public boolean isSaveData() {
            return saveData;
        }

        public void setSaveData(boolean saveData) {
            this.saveData = saveData;
        }

        @Override
        public String toString() {
            return "Config{" +
                    "saveData=" + saveData +
                    '}';
        }

        public Config(boolean saveData, boolean enableActionsNotifications) {
            this.saveData = saveData;
            this.enableActionsNotifications = enableActionsNotifications;
        }
    }

    Config engineConfig;

    public Config getEngineConfig() {
        return engineConfig;
    }

    public void setEngineConfig(Config engineConfig) {
        this.engineConfig = engineConfig;
    }

    @Override
    public String toString() {
        return "MetricsMessage{" +
                "metricsDefinition=" + metricsDefinition +
                ", metric='" + metric + '\'' +
                ", context=" + context +
                ", dataFilesDir='" + dataFilesDir + '\'' +
                '}';
    }
}
