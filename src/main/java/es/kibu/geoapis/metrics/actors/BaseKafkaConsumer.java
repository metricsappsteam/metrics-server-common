package es.kibu.geoapis.metrics.actors;

import akka.NotUsed;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.japi.function.Function;
import akka.kafka.ConsumerSettings;
import akka.kafka.KafkaConsumerActor;
import akka.kafka.Subscriptions;
import akka.kafka.javadsl.Consumer;
import akka.stream.*;
import akka.stream.javadsl.FileIO;
import akka.stream.javadsl.Flow;
import akka.stream.javadsl.Keep;
import akka.stream.javadsl.Sink;
import akka.util.ByteString;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Paths;
import java.util.concurrent.CompletionStage;

import static es.kibu.geoapis.metrics.actors.KafkaConsts.KAFKA_BOOTSTRAP_SERVERS;

public abstract class BaseKafkaConsumer<T> {

    private String topic /*= "metrics_data"*/;

    static Logger logger = LoggerFactory.getLogger(BaseKafkaConsumer.class);

    public BaseKafkaConsumer(String topic) {
        this.topic = topic;
    }

    //region How to use rest kafka services
/*   # Produce a message using JSON with the value '{ "foo": "bar" }' to the topic test
    $ curl -X POST -H "Content-Type: application/vnd.kafka.json.v1+json" \
            --data '{"records":[{"value":{"foo":"bar"}}]}' "http://localhost:8082/topics/jsontest"
    {"offsets":[{"partition":0,"offset":0,"error_code":null,"error":null}],"key_schema_id":null,"value_schema_id":null}

    # Create a consumer for JSON data, starting at the beginning of the topic's
    # log. Then consume some data from a topic using the base URL in the first response.

    # Finally, close the consumer with a DELETE to make it leave the group and clean up
    # its resources.
    $ curl -X POST -H "Content-Type: application/vnd.kafka.v1+json" \
            --data '{"name": "my_consumer_instance", "format": "json", "auto.offset.reset": "smallest"}' \
    http://localhost:8082/consumers/my_json_consumer
    {"instance_id":"my_consumer_instance",
            "base_uri":"http://localhost:8082/consumers/my_json_consumer/instances/my_consumer_instance"}
    $ curl -X GET -H "Accept: application/vnd.kafka.json.v1+json" \
    http://localhost:8082/consumers/my_json_consumer/instances/my_consumer_instance/topics/jsontest
            [{"key":null,"value":{"foo":"bar"},"partition":0,"offset":0}]
    $ curl -X DELETE \
    http://localhost:8082/consumers/my_json_consumer/instances/my_consumer_instance

    */
    //endregion

    public void start(ActorSystem system, String configResourceBaseName, Sink<T, NotUsed> sink) {

        final ConsumerSettings<String, String> consumerSettings = getConsumerSettings(system, configResourceBaseName);
        start(system, sink, consumerSettings);

    }

    public void start(ActorSystem system, Config config, Sink<T, NotUsed> sink) {
        final ConsumerSettings<String, String> consumerSettings = getConsumerSettings(system, config);
        start(system, sink, consumerSettings);
    }

    private void start(ActorSystem system, Sink<T, NotUsed> sink, ConsumerSettings<String, String> consumerSettings) {
        final Function<Throwable, Supervision.Directive> decider = exc -> {
            //if (exc instanceof ArithmeticException)
            return Supervision.resume();
                /*else
                    return Supervision.stop();*/
        };

        //Function<Throwable, Supervision.Directive> decider1 = decider;
        final Materializer materializer = ActorMaterializer.create(ActorMaterializerSettings.create(system).withSupervisionStrategy(decider), system);

        //Consumer is represented by actor
        ActorRef consumer = system.actorOf((KafkaConsumerActor.props(consumerSettings)));

        logger.debug("Creating the sink ...");

        final Sink<T, NotUsed> writeVariableSink = sink/*cassandraSinkActor(system)*/;

        //Manually assign topic partition to it
        logger.debug("Creating consumer ...");

        Consumer
                .plainExternalSource(consumer, Subscriptions.assignment(new TopicPartition(
                        topic, 0)/**/))
                .map(t -> {
                    logger.debug("Mapping ...");
                    String key = (String)t.key();
                    logger.debug("processing record topic: {}, key: {}, value: {}", t.topic(), key, t.value());
                    T value = encode(t);
                    logger.debug("Instruction: {}", value);
                    return value;
                })
                .runWith(writeVariableSink, materializer);
    }

    protected abstract T encode(ConsumerRecord<Object, Object> record);

    private ConsumerSettings<String, String> getConsumerSettings(ActorSystem system, String configResourceBaseName) {
        Config config = ConfigFactory.load(configResourceBaseName);
        return getConsumerSettings(system, config);
    }

    protected String getBootstrapServersKey(){
        return  KAFKA_BOOTSTRAP_SERVERS;
    }

    private ConsumerSettings<String, String> getConsumerSettings(ActorSystem system, Config outConfig) {
        if (outConfig != null) {
            Config config = outConfig.getConfig("akka.kafka.consumer");
            String bootstrapServers = outConfig.getString(getBootstrapServersKey());
            logger.debug("Creating consumer settings from: {} ", "config");
            return ConsumerSettings.create(config/*system*/, new StringDeserializer(), new StringDeserializer())
                    .withBootstrapServers(bootstrapServers)
                    //.withGroupId("group1")
                    .withProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        } else {
            logger.debug("Creating consumer settings from ActorSystem");
            String bootstrapServers = system.settings().config().getString("bootstrap.servers");
            return ConsumerSettings.create(system, new StringDeserializer(), new StringDeserializer())
                    .withBootstrapServers(bootstrapServers)
                    //.withGroupId("group1")
                    .withProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        }
    }

/*
    private String getData(ConsumerRecord t) {
        return t.value().toString();
    }
*/

/*

    public static class KeyDetails implements Serializable {
        String variable;
        String application;

        public KeyDetails(String variable, String application) {
            this.variable = variable;
            this.application = application;
        }
    }

    public static KeyDetails decodeKey(String key) {
        logger.debug("Key to decode: {}", key);
        if (key.startsWith("\"") && key.endsWith("\"")) {
            key = key.substring(1, key.length() - 1);
        }

        logger.debug("Cleaned key: {}", key);

        String[] split = key.split(":");
        String application = split[0];
        String variable = split[1];
        return new KeyDetails(variable, application);
    }
*/

    public Sink<String, CompletionStage<IOResult>> lineSink(String filename) {
        return Flow.of(String.class)
                .map(s -> ByteString.fromString(s.toString() + "\n"))
                .toMat(FileIO.toPath(Paths.get(filename)), Keep.right());
    }
}
