package es.kibu.geoapis.metrics.actors;

import akka.NotUsed;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.stream.javadsl.Flow;
import akka.stream.javadsl.Keep;
import akka.stream.javadsl.Sink;

import java.io.Serializable;

public class KafkaConsumerUtils {

    public static class CompleteObject implements Serializable {

    }

    public static class EventStreamSinkHelper {

        static ActorSystem actorSystem;

        public static <M> void sendToEventStream(M message) {
            if (actorSystem != null) {
                actorSystem.eventStream().publish(message);
            }
        }

        static void withActorSystem(ActorSystem system) {
            EventStreamSinkHelper.actorSystem = system;
        }

    }


    public static <M> Sink<M, NotUsed> eventStreamSink(ActorSystem system, Class<M> classOfM) {
        EventStreamSinkHelper.withActorSystem(system);
        /*final Sink<MetricsDefinitionCacheActor.MetricsDefinitionActorMessage, NotUsed> sink = Flow.of(MetricsDefinitionCacheActor.MetricsDefinitionActorMessage.class)
                .to(Sink.foreach(SentToEventStream::sendToEventStream));*/
        final Sink<M, NotUsed> sink = Flow.of(classOfM)
                .to(Sink.foreach(EventStreamSinkHelper::sendToEventStream));

        return Flow.of(classOfM)
                .toMat(sink, Keep.none());
    }


    public static <M> Sink<M, NotUsed> actorSink(Class<M> clazz, ActorRef actorRef) {
        return Flow.of(clazz)
                .toMat(Sink.actorRef(actorRef, new KafkaConsumerUtils.CompleteObject()), Keep.none());
    }



}
