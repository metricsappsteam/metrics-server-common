package es.kibu.geoapis.metrics.actors.data;

import es.kibu.geoapis.objectmodel.MetricsDefinition;

import java.io.Serializable;

/**
 * Created by lrodriguez2002cu on 28/12/2016.
 */

public class WriteInstruction implements Serializable {

    public static class WriteContext implements Serializable{

        String application;
        String user;
        String session;
        String variable;
        String metricId;

        public String getVariable() {
            return variable;
        }

        public void setVariable(String variable) {
            this.variable = variable;
        }

        public String getMetricId() {
            return metricId;
        }

        public void setMetricId(String metricId) {
            this.metricId = metricId;
        }

        public WriteContext(String application, String user, String session, String variable, String metricId) {
            this.application = application;
            this.user = user;
            this.session = session;
            this.variable = variable;
            this.metricId = metricId;
        }

        public static WriteContext failed() {
            return new WriteInstruction.WriteContext("fail", "fail", "fail", "fail", "fail");
        }

        public String getApplication() {
            return application;
        }

        public void setApplication(String application) {
            this.application = application;
        }

        public String getUser() {
            return user;
        }

        public void setUser(String user) {
            this.user = user;
        }

        public String getSession() {
            return session;
        }

        public void setSession(String session) {
            this.session = session;
        }
    }

    String  variable;
    String data;
    MetricsDefinition definition;
    WriteContext context;

    public WriteContext getContext() {
        return context;
    }

    public void setContext(WriteContext context) {
        this.context = context;
    }

    public MetricsDefinition getDefinition() {
        return definition;
    }

    public void setDefinition(MetricsDefinition definition) {
        this.definition = definition;
    }

    public String getMetricId() {
         return isResolved()? definition.getId() : null;
    }

    public WriteInstruction(String variable, String data, WriteContext context) {
        //logger.debug("Creating WriteInstruction...");
        this.variable = variable;
        this.data = data;
        this.context = context;
    }

    public String getVariable() {
        return variable;
    }

    public void setVariable(String variable) {
        this.variable = variable;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public boolean isResolved(){
        return definition != null;
    }

    @Override
    public String toString() {
        return "WriteInstruction{" +
                "variable=" + variable +
                ", data='" + data + '\'' +
                '}';
    }
}
