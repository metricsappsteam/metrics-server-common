package es.kibu.geoapis.services;

import akka.actor.ActorSystem;
import es.kibu.geoapis.services.objectmodel.results.ExistResult;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Optional;

/**
 * Created by lrodriguez2002cu on 14/01/2017.
 */
public class MetricsManager extends EntityManager<MetricsStorageUtils.MetricsDef> {

    protected MetricsManager(ActorSystem system) {
        super(system);
    }

    protected String getCollection() {
        return MetricsStorageUtils.METRICS_DEF_COLLECTION;
    }

    public static String getHash(String content) throws NoSuchAlgorithmException {
        MessageDigest md5 = MessageDigest.getInstance("MD5");
        md5.update(StandardCharsets.UTF_8.encode(content));
        return String.format("%032x", new BigInteger(1, md5.digest()));
    }

    public Optional<MetricsStorageUtils.MetricsDef> getMetricsDef(String applicationId) {
        return super.getEntity(applicationId, MetricsStorageUtils.MetricsDef.class);
    }

    public Object updateMetricsDef(String applicationId, MetricsStorageUtils.MetricsDef metricsDef) {
        return super.updateEntity(applicationId, metricsDef);
    }

    public Object deleteMetricsDef(String applicationId) {
        return super.deleteEntity(applicationId);
    }

    public Object createMetricsDef(String applicationId, MetricsStorageUtils.MetricsDef metricsDef) {

        return super.createEntity(applicationId, metricsDef);
    }

    protected void checkCreation(MetricsStorageUtils.MetricsDef def) {
        assert def.getContent() != null;
        assert !def.getContent().isEmpty();

        assert def.getHash() != null;
        assert def.getMetricId() != null;
    }


    protected void checkUpdate(MetricsStorageUtils.MetricsDef def) {
        assertTrue(def.getContent() != null, "The content must not be empty");
        assertTrue(!def.getContent().isEmpty(), "The content must not be empty");

        assertTrue(def.getHash() != null, "The hash must be calculated before updating or creating the metric for the app.");
        assertTrue(def.getMetricId() != null, "The metric id must be provided");
    }


    public ExistResult metricsExistsForApp(String applicationId) {
        return super.entityExistsForApp(applicationId);
    }


    @Override
    protected Class<MetricsStorageUtils.MetricsDef> getEntityClass() {
        return MetricsStorageUtils.MetricsDef.class;
    }
}
