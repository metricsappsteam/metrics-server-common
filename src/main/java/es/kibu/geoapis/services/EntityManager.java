package es.kibu.geoapis.services;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.util.Timeout;
import es.kibu.geoapis.backend.actors.ActorsUtils;
import es.kibu.geoapis.backend.actors.messages.ApplicationId;
import es.kibu.geoapis.backend.actors.messages.EntityId;
import es.kibu.geoapis.backend.actors.messages.Identifiable;
import es.kibu.geoapis.backend.actors.persistence.AbstractQueries;
import es.kibu.geoapis.backend.actors.persistence.PersistenceActor;
import es.kibu.geoapis.backend.actors.persistence.PersistenceMasterActor;
import es.kibu.geoapis.backend.actors.persistence.PersistenceMessageUtils;
import es.kibu.geoapis.services.objectmodel.results.ExistResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scala.concurrent.duration.FiniteDuration;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

/**
 * Created by lrodr_000 on 14/01/2017.
 */
public abstract class EntityManager<T extends Identifiable<String>> {

    ActorSystem system;
    static ActorRef persistenceActorRef;
    Logger logger = LoggerFactory.getLogger(EntityManager.class);
    private Timeout timeout  = Timeout.apply(new FiniteDuration(10, TimeUnit.SECONDS));

    protected ActorSystem getSystem() {
        return system;
    }

    public EntityManager(ActorSystem system) {
        this.system = system;
        //this.persistenceActorRef = null;
    }


    protected ActorRef getPersistenceActorRef() {
        if (persistenceActorRef == null || persistenceActorRef.isTerminated()) {
            persistenceActorRef = ActorsUtils.getPersistenceActor(system);
            //getSystem().actorOf(PersistenceMasterActor.props(), "entity-manager-persistence-actor" + hashCode());
        }
        return persistenceActorRef;
    }

    /**
     * These methods (checkCreate and  checkUpdate) provide a way of validating data passed to the entity manager.
     * @param obj
     */
    protected abstract void checkCreation(T obj);

    /**
     * These methods (checkCreate and  checkUpdate) provide a way of validating data passed to the entity manager.
     * @param obj
     */
    protected abstract void checkUpdate(T obj);

    protected void assertTrue(boolean condition, String message) {
        if (!condition) throw new MetricsStorageUtils.MetricsServiceLogicException(message);
    }

    public Optional<T> getEntity(String applicationId, Class<T> clazz) {
        final ActorRef persistenceActor = getPersistenceActorRef();
        logger.debug("Retrieving entity for class: '{}', app: '{}'", getEntityClass().getCanonicalName(), applicationId);
        AbstractQueries.Query selectAllQueryForApp = PersistenceMessageUtils.getSelectAllQueryForApp(getCollection(), clazz, new ApplicationId(applicationId));
        PersistenceActor.QueryResult queryResult = PersistenceMessageUtils.performSelect(selectAllQueryForApp, persistenceActor, timeout);
        if (PersistenceMessageUtils.hasRecords(queryResult)) {
            T metricsDef = PersistenceMessageUtils.getUniqueRecord(queryResult);
            return Optional.of(metricsDef);
        } else {
            logger.debug("No record found, response was empty.");
            return Optional.empty();
        }
    }

    public Optional<List<T>> getEntities(Class<T> clazz) {
        final ActorRef persistenceActor = getPersistenceActorRef();
        AbstractQueries.Query selectAllQueryForApp = new AbstractQueries.SelectQuery(clazz, getCollection());
        PersistenceActor.QueryResult queryResult = PersistenceMessageUtils.performSelect(selectAllQueryForApp, persistenceActor, timeout);
        if (PersistenceMessageUtils.hasRecords(queryResult)) {
            List<T> records = PersistenceMessageUtils.getRecords(queryResult);
            return Optional.of(records);
        } else {
            return Optional.empty();
        }
    }

    public Object updateEntity(String applicationId, T metricsDef) {
        Object result = createOrUpdateEntity(applicationId, metricsDef, false);
        return result;
    }

    public Object deleteEntity(String applicationId) {
        ExistResult existResult = entityExistsForApp(applicationId);
        if (existResult.isResult()) {
            String id = existResult.getId();
            AbstractQueries.Query query = new AbstractQueries.DeleteQuery(new EntityId(id), getCollection());
            PersistenceMessageUtils.queryWithApplicationId(query, new ApplicationId(applicationId));
            ActorRef persistenceActor = getPersistenceActorRef();
            boolean deleted = PersistenceMessageUtils.performDeletionQuery(query, persistenceActor, timeout);
            return deleted;
        } else return false;
    }

    public Object createEntity(String applicationId, T metricsDef) {
        Object result = createOrUpdateEntity(applicationId, metricsDef, true);
        return result;
    }

    protected Object createOrUpdateEntity(String applicationId, T metricsDef, boolean create) {

        if (create) {
            checkCreation(metricsDef);
        } else {
            checkUpdate(metricsDef);
        }

        final ActorRef persistenceActor = getPersistenceActorRef();
        AbstractQueries.Query query =
                (!create) ?
                        new AbstractQueries.UpdateQuery(metricsDef, getCollection())
                        : new AbstractQueries.InsertQuery(metricsDef, getCollection());

        query.withFilter(PersistenceMessageUtils.applicationIdFilter(new ApplicationId(applicationId))).build();
        if (!create) {
            PersistenceMessageUtils.queryWithId(query, new EntityId(metricsDef.getId()));
        }
        return (!create) ?
                PersistenceMessageUtils.performUpdate(query, persistenceActor, timeout) :
                PersistenceMessageUtils.performCreate(metricsDef, query, persistenceActor, timeout);
    }

    protected abstract String getCollection();

    protected abstract Class<T> getEntityClass();

    public  ExistResult entityExistsForApp(String applicationId) {
           return entityExistsForApp(applicationId, getEntityClass());
    }

    protected ExistResult entityExistsForApp(String applicationId, Class<T> clazz) {
        Optional<T> entity = getEntity(applicationId, clazz);
        if (entity.isPresent()) {
            T entityObj = entity.get();
            String hash = (entityObj instanceof  HashedEntity)? ((HashedEntity)entityObj).getHash(): "";
            return new ExistResult(true, hash, entityObj.getId());
        } else return new ExistResult(false, "", "");
    }

}
