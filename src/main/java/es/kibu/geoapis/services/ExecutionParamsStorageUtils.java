package es.kibu.geoapis.services;

import akka.actor.ActorSystem;

/**
 * Created by lrodriguez2002cu on 14/01/2017.
 */
public class ExecutionParamsStorageUtils {

    public static class ExecutionParamsServiceLogicException extends RuntimeException {
        public ExecutionParamsServiceLogicException(String message) {
            super(message);
        }
    }

    public static ExecutionParamsManager getExecutionParamsManager(ActorSystem system) {
        return new ExecutionParamsManager(system);
    }

}
