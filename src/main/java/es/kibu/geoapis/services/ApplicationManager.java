package es.kibu.geoapis.services;

import akka.actor.ActorSystem;
import es.kibu.geoapis.backend.actors.messages.Identifiable;
import es.kibu.geoapis.services.objectmodel.results.Application;
import es.kibu.geoapis.services.objectmodel.results.ExistResult;

import java.util.List;
import java.util.Optional;

/**
 * Created by lrodr_000 on 14/01/2017.
 */
public class ApplicationManager extends EntityManager<ApplicationManager.ApplicationModel> {


    public static final String APPLICATIONS_COLLECTION = "applications";

    public ApplicationManager(ActorSystem system) {
        super(system);
    }

    public static class ApplicationModel extends Application implements Identifiable<String> {

        public void fromApp(Application application){
            this.assign(application);
        }

        public void setId(String  id) {
            super.setId(id);
        }
    }

    public static class ApplicationServiceLogicException extends RuntimeException {
        public ApplicationServiceLogicException (String message) {
            super(message);
        }
    }

    @Override
    protected void checkCreation(ApplicationModel obj) {

    }

    @Override
    protected void checkUpdate(ApplicationModel obj) {

    }

    @Override
    protected String getCollection() {
        return APPLICATIONS_COLLECTION;
    }

    public Object updateApplication(String applicationId, ApplicationModel applicationModel) {
        return super.updateEntity(applicationId, applicationModel);
    }

    public Object deleteApplication(String applicationId) {
        return super.deleteEntity(applicationId);
    }

    public Object createApplication(String applicationId, ApplicationModel applicationModel) {
        return super.createEntity(applicationId, applicationModel);
    }

    @Override
    protected Class<ApplicationModel> getEntityClass() {
        return ApplicationModel.class;
    }

    public ExistResult applicationExists(String applicationId) {
        ExistResult existResult = super.entityExistsForApp(applicationId);
        return existResult;
    }

    public Optional<ApplicationModel> getApplication(String applicationId) {
        return super.getEntity(applicationId, getEntityClass());
    }

    public Optional<List<ApplicationModel>> getApplications() {
        return super.getEntities(getEntityClass());
    }

}
