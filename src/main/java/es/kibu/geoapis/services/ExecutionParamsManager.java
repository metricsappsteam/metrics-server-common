package es.kibu.geoapis.services;

import akka.actor.ActorSystem;
import es.kibu.geoapis.backend.actors.messages.Identifiable;
import es.kibu.geoapis.services.objectmodel.results.ExecutionParams;
import es.kibu.geoapis.services.objectmodel.results.ExistResult;

import java.util.Optional;

/**
 * Created by lrodr_000 on 14/01/2017.
 */
public class ExecutionParamsManager extends EntityManager<ExecutionParamsManager.ExecutionParamsModel> {

    public static final String EXECUTIONPARAMS_COLLECTION = "executionParams";

    public ExecutionParamsManager(ActorSystem system) {
        super(system);
    }

    public static class ExecutionParamsModel extends ExecutionParams implements Identifiable<String> {
        public void fromExecutionParams(ExecutionParams executionParams) {
            this.assign(executionParams);
        }

        @Override
        public void setId(String  id) {
            super.setId(id);
        }
    }

    public static class ExecutionParamServiceLogicException extends RuntimeException {
        public ExecutionParamServiceLogicException(String message) {
            super(message);
        }
    }

    @Override
    protected void checkCreation(ExecutionParamsModel obj) {

    }

    @Override
    protected void checkUpdate(ExecutionParamsModel obj) {

    }

    @Override
    protected String getCollection() {
        return EXECUTIONPARAMS_COLLECTION;
    }

    public Object updateExecutionParams(String applicationId, ExecutionParamsModel executionParamsModel) {
        return super.updateEntity(applicationId, executionParamsModel);
    }

    public Object deleteExecutionParams(String applicationId) {
        return super.deleteEntity(applicationId);
    }

    public Object createExecutionParams(String applicationId, ExecutionParamsModel executionParamsModel) {
        return super.createEntity(applicationId, executionParamsModel);
    }

    @Override
    protected Class<ExecutionParamsModel> getEntityClass() {
        return ExecutionParamsModel.class;
    }

    public ExistResult executionParamsExists(String applicationId) {
        ExistResult existResult = super.entityExistsForApp(applicationId);
        return existResult;
    }

    public Optional<ExecutionParamsModel> getExecutionParams(String applicationId) {
        return super.getEntity(applicationId, getEntityClass());
    }

}
