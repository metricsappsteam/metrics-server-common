package es.kibu.geoapis.services;

import akka.actor.ActorSystem;
import es.kibu.geoapis.backend.actors.messages.Identifiable;
import es.kibu.geoapis.services.objectmodel.results.MetricsResult;

/**
 * Created by lrodr_000 on 14/01/2017.
 */
public class ApplicationStorageUtils {

    public static class ApplicationServiceLogicException extends RuntimeException {
        public ApplicationServiceLogicException(String message) {
            super(message);
        }
    }

    public static ApplicationManager getApplicationManager(ActorSystem system) {
        return new ApplicationManager(system);
    }

}
