package es.kibu.geoapis.services;

/**
 * Created by lrodr_000 on 14/01/2017.
 */
public interface HashedEntity {
    String getHash();
}
