package es.kibu.geoapis.services.data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lrodr_000 on 27/04/2017.
 */
public class DataSources {

    public static class VariablesDatasourceInfo {
        String variableName;
        String datasourceName;
        String application;

        public String getVariableName() {
            return variableName;
        }

        public String getDatasourceName() {
            return datasourceName;
        }

        public String getApplication() {
            return application;
        }

        public VariablesDatasourceInfo(String variableName, String datasourceName, String application) {
            this.variableName = variableName;
            this.datasourceName = datasourceName;
            this.application = application;
        }

        public VariablesDatasourceInfo() {
        }
    }

    public static class MetricsDatasourceInfo {
        String metricsName;
        String datasourceName;
        String application;

        public String getMetricsName() {
            return metricsName;
        }

        public String getDatasourceName() {
            return datasourceName;
        }

        public String getApplication() {
            return application;
        }

        public MetricsDatasourceInfo(String metricsName, String datasourceName, String application) {
            this.metricsName = metricsName;
            this.datasourceName = datasourceName;
            this.application = application;

        }

        public MetricsDatasourceInfo() {
        }
    }


    List<VariablesDatasourceInfo> variablesDatasources;
    List<MetricsDatasourceInfo> metricsDatasources;

    public List<VariablesDatasourceInfo> getVariablesDatasources() {
        return variablesDatasources;
    }

    public List<MetricsDatasourceInfo> getMetricsDatasources() {
        return metricsDatasources;
    }

    public DataSources() {
        variablesDatasources = new ArrayList<>();
        metricsDatasources = new ArrayList<>();
    }

    public void addMetricDatasource(MetricsDatasourceInfo info) {
        metricsDatasources.add(info);
    }

    public void addVariableDatasource(VariablesDatasourceInfo info) {
        variablesDatasources.add(info);
    }


}
