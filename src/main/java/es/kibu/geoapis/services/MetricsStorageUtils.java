package es.kibu.geoapis.services;

import akka.actor.ActorSystem;
import es.kibu.geoapis.backend.actors.messages.Identifiable;
import es.kibu.geoapis.services.objectmodel.results.MetricsResult;

/**
 * Created by lrodriguez2002cu on 29/12/2016.
 */
public class MetricsStorageUtils {

    public static final String METRICS_DEF_COLLECTION = "metrics_defs";


    /**
     * Note: this  is a duplicated code from es.kibu.geoapis.services.objectmodel.results.MetricsResult,
     * and it is done in this way for a reason, the id of identifiable and the id of MetricsResults enter in conflict so
     * that while deserializing by PersistenceMongo actor,  the following problem arieses:
     *
     * used by: java.lang.IllegalArgumentException: Conflicting setter definitions for property "id":
     * es.kibu.geoapis.services.objectmodel.results.MetricsResult#setId(1 params) vs es.kibu.geoapis.backend.actors.messages.Identifiable#setId(1 params)
     *
     */
    public static class MetricsDef extends MetricsResult implements Identifiable<String> , HashedEntity {

       /* String content;
        String hash;
        String metricId;
        *//*boolean is_public;*//*
        String id;
        boolean isPublic;

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getHash() {
            return hash;
        }

        public void setHash(String hash) {
            this.hash = hash;
        }

        public String getMetricId() {
            return metricId;
        }

        public void setMetricId(String metricId) {
            this.metricId = metricId;
        }
*/
        public String getId() {
            return super.getId();
        }

        public void setId(String id) {
            super.setId(id);
        }
/*
        public boolean isIs_public() {
            return is_public;
        }

        public void setIs_public(boolean is_public) {
            this.is_public = is_public;
        }*/


      /*  public boolean getIsPublic() {
            return this.isPublic;
        }

        public void setIsPublic(boolean aPublic) {
            this.isPublic = aPublic;
        }

        public MetricsDef() {
        }

        public MetricsDef(String content, String hash, String metricId, boolean is_public) {
            this.content = content;
            this.hash = hash;
            this.metricId = metricId;
            this.isPublic = is_public;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
*/
     /*   @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            MetricsResult that = (MetricsResult) o;

            if (getIsPublic() != that.getIsPublic()) return false;
            if (getContent() != null ? !getContent().equals(that.getContent()) : that.getContent() != null) return false;
            if (getHash() != null ? !getHash().equals(that.getHash()) : that.getHash() != null) return false;
            if (getMetricId() != null ? !getMetricId().equals(that.getMetricId()) : that.getMetricId() != null)
                return false;
            return getId() != null ? getId().equals(that.getId()) : that.getId() == null;
        }

        @Override
        public int hashCode() {
            int result = getContent() != null ? getContent().hashCode() : 0;
            result = 31 * result + (getHash() != null ? getHash().hashCode() : 0);
            result = 31 * result + (getMetricId() != null ? getMetricId().hashCode() : 0);
            result = 31 * result + (getIsPublic() ? 1 : 0);
            result = 31 * result + (getId() != null ? getId().hashCode() : 0);
            return result;
        }
*/
        public MetricsDef(String content, String hash, String metricId, boolean isPublic) {
            this.setContent(content);
            this.setHash(hash);
            this.setMetricId(metricId);
            this.setIsPublic(isPublic);
        }

        //important as it is uses for creating the instance after deserializing
        public MetricsDef() {
        }
    }

    public static class MetricsServiceLogicException extends RuntimeException {
        public MetricsServiceLogicException(String message) {
            super(message);
        }
    }

    public static MetricsManager getMetricsManager(ActorSystem system) {
        return new MetricsManager(system);
    }


}
