package es.kibu.geoapis.metrics.actors.data;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.testkit.JavaTestKit;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import es.kibu.geoapis.services.MetricsManager;
import es.kibu.geoapis.services.MetricsStorageUtils;
import mockit.Capturing;
import mockit.Expectations;
import mockit.Mocked;
import mockit.Verifications;
import mockit.integration.junit4.JMockit;
import org.apache.commons.io.IOUtils;
import org.ehcache.Cache;
import org.junit.*;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scala.concurrent.duration.FiniteDuration;

import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

import static org.junit.Assert.assertTrue;

/**
 * Created by lrodriguez2002cu on 29/12/2016.
 */
@RunWith(JMockit.class)

public class MetricsDefinitionCacheActorTest {

    private static ActorSystem system;
    Logger logger = LoggerFactory.getLogger(MetricsDefinitionCacheActorTest.class);
    static String metricString;


    @BeforeClass
    public static void setUp() throws Exception {
        system = ActorSystem.create();
        InputStream resourceAsStream = MetricsDefinitionCacheActorTest.class.getClassLoader().getResourceAsStream("sample-metrics/sample-metric-wikiloc-box-speed.json");
        metricString = IOUtils.toString(resourceAsStream, "UTF-8");

    }

    @Mocked
    MetricsManager manager;

    @Test
    public void testCacheActorDefinitionStoreWorks(@Capturing Cache<String, String> metricsDefinitionCache) throws IOException, ProcessingException {

        final String appid = "mocked-app";
        final String metricid = "mocked-metricid";

        test(metricsDefinitionCache, appid, metricid);
    }

    private void test(@Capturing final Cache<String, String> metricsDefinitionCache, final String appid, final String metricid) throws IOException {

        // final MetricsDefinition metricsDefinition = MetricUtils.withMetricDefinition("sample-metrics/sample-metric-wikiloc-box-speed.json");

        new Expectations() {{ // an "expectation block"
            // Record an expectation, with a given value to be returned:
            //MetricsStorageUtils.getMetricsManager((ActorSystem) any)

            manager.getMetricsDef((String) any);
            result = Optional.of(new MetricsStorageUtils.MetricsDef(metricString, "samplehash", "sampleMetricId", true));

            //only one time is put a new definition in the cache
            metricsDefinitionCache.put(anyString/*MetricsDefinitionCacheActor.CacheKey.of(appid, metricid)*/, anyString);
            times = 1;

            //because next time it says is already there (catched)
            metricsDefinitionCache.containsKey(anyString);
            returns(false, true, true);

        }};


        new JavaTestKit(system) {{
            final Props props = MetricsDefinitionCacheActor.props();
            final ActorRef subject = system.actorOf(props);

            //system.eventStream().subscribe(subject, MetricsDefinitionCacheActor.MetricsDefinitionActorMessage.class);
            // can also use JavaTestKit “from the outside”
            final JavaTestKit probe = new JavaTestKit(system);

            // the run() method needs to finish within 3 seconds

            MetricsDefinitionCacheActor.MetricsDefinitionActorMessage msg = new MetricsDefinitionCacheActor.MetricsDefinitionActorMessage(appid, metricid, MetricsDefinitionCacheActor.Action.GET);

            final FiniteDuration duration = duration("40 seconds");

            new Within(duration) {
                protected void run() {
                    subject.tell(msg, getRef());
                    // Will wait for the rest of the 3 seconds
                    MetricsDefinitionCacheActor.MetricsDefinitionActorReplyMessage replyMessage = expectMsgClass(MetricsDefinitionCacheActor.MetricsDefinitionActorReplyMessage.class);

                    assertTrue(replyMessage.application == appid);
                    assertTrue(replyMessage.metricId == metricid);
                }
            };

            new Within(duration) {
                protected void run() {
                    subject.tell(msg, getRef());
                    // Will wait for the rest of the 3 seconds
                    MetricsDefinitionCacheActor.MetricsDefinitionActorReplyMessage replyMessage = expectMsgClass(MetricsDefinitionCacheActor.MetricsDefinitionActorReplyMessage.class);

                    assertTrue(replyMessage.application == appid);
                    assertTrue(replyMessage.metricId == metricid);
                }
            };

            //does it works with the events stream
            new Within(duration) {
                protected void run() {
                    system.eventStream().publish(msg);
                    // Will wait for the rest of the 3 seconds
                    //MetricsDefinitionCacheActor.MetricsDefinitionActorReplyMessage replyMessage = expectMsgClass(MetricsDefinitionCacheActor.MetricsDefinitionActorReplyMessage.class);

                    //assertTrue(replyMessage.application == appid);
                    //assertTrue(replyMessage.metricId == metricid);
                }
            };

        }};

        new Verifications() {{
            //is asked twice by the existence of the key in the cache
            metricsDefinitionCache.containsKey(MetricsDefinitionCacheActor.CacheKey.of(appid, metricid));
            times = 3;

        }};
    }

    @Test
    @Ignore
    public void testCacheActorDefinitionStoreWorksNoMetricId(@Capturing Cache<String, String> metricsDefinitionCache) throws IOException, ProcessingException {

        final String appid = "mocked-app";
        final String metricid = null/*"mocked-metricid"*/;

        test(metricsDefinitionCache, appid, metricid);
    }

    @Test
    public void testCacheActorRepliesWithErrorIfUnknownAppAndMetricIsRequested() throws IOException, ProcessingException {

        final String appid = "mocked-app";
        final String metricid = "mocked-metricid";

        testRepliesWithError(appid, metricid);
    }

    @Test
    public void testCacheActorRepliesWithErrorIfUnknownAppAndMetricIsRequested1() throws IOException, ProcessingException {

        final String appid = "mocked-app";
        final String metricid = null;/*"mocked-metricid"*/

        testRepliesWithError(appid, metricid);
    }

    private void testRepliesWithError(final String appid, final String metricid) throws IOException {
        InputStream resourceAsStream = MetricsDefinitionCacheActorTest.class.getClassLoader().getResourceAsStream("sample-metrics/sample-metric-wikiloc-box-speed.json");
        String metricString = IOUtils.toString(resourceAsStream, "UTF-8");

        // final MetricsDefinition metricsDefinition = MetricUtils.withMetricDefinition("sample-metrics/sample-metric-wikiloc-box-speed.json");

        new Expectations() {{

            manager.getMetricsDef((String) any);
            times = 1;
            returns(Optional.of(new MetricsStorageUtils.MetricsDef(metricString, "samplehash", "sampleMetricId", true)),
                    Optional.empty());
        }};


        new JavaTestKit(system) {{

            final Props props = MetricsDefinitionCacheActor.props();
            final ActorRef subject = system.actorOf(props);

            // can also use JavaTestKit “from the outside”
            final JavaTestKit probe = new JavaTestKit(system);

            // the run() method needs to finish within 3 seconds

            MetricsDefinitionCacheActor.MetricsDefinitionActorMessage msg = new MetricsDefinitionCacheActor.MetricsDefinitionActorMessage(appid, metricid, MetricsDefinitionCacheActor.Action.GET);

            final FiniteDuration duration = duration("40 seconds");

            new Within(duration) {
                protected void run() {
                    subject.tell(msg, getRef());
                    // Will wait for the rest of the 3 seconds
                    MetricsDefinitionCacheActor.MetricsDefinitionActorReplyMessage replyMessage = expectMsgClass(MetricsDefinitionCacheActor.MetricsDefinitionActorReplyMessage.class);

                    assertTrue(replyMessage.application == appid);
                    assertTrue(replyMessage.metricId == metricid);
                }
            };

            new Within(duration) {
                protected void run() {
                    subject.tell(msg, getRef());
                    // Will wait for the rest of the 3 seconds
                    MetricsDefinitionCacheActor.MetricsDefinitionActorReplyMessage replyMessage = expectMsgClass(MetricsDefinitionCacheActor.MetricsDefinitionActorReplyMessage.class);

                    assertTrue(replyMessage.application == appid);
                    assertTrue(replyMessage.metricId == metricid);
                }
            };


            String unknown_appid = "unknown_appid";
            String unknown_metricid = "unknown_metricid";
            final MetricsDefinitionCacheActor.MetricsDefinitionActorMessage msg1 = new MetricsDefinitionCacheActor.MetricsDefinitionActorMessage(unknown_appid, unknown_metricid, MetricsDefinitionCacheActor.Action.GET);
            new Within(duration) {
                protected void run() {
                    subject.tell(msg1, getRef());
                    // Will wait for the rest of the 3 seconds
                    MetricsDefinitionCacheActor.MetricsDefinitionActorReplyMessage replyMessage = expectMsgClass(MetricsDefinitionCacheActor.MetricsDefinitionActorReplyMessage.class);

                    assertTrue(replyMessage.application == unknown_appid);
                    assertTrue(replyMessage.metricId == unknown_metricid);

                    assertTrue(replyMessage.isError());
                }
            };

        }};

        new Verifications() {{

        }};
    }

    @Test
    public void testCacheActorInvalidationWorks() throws IOException, ProcessingException {

        InputStream resourceAsStream = MetricsDefinitionCacheActorTest.class.getClassLoader().getResourceAsStream("sample-metrics/sample-metric-wikiloc-box-speed.json");
        String metricString = IOUtils.toString(resourceAsStream, "UTF-8");
        // final MetricsDefinition metricsDefinition = MetricUtils.withMetricDefinition("sample-metrics/sample-metric-wikiloc-box-speed.json");

        new Expectations() {{

            //this should be called twice one the first time and another after invalidating the cache
            manager.getMetricsDef((String) any);
            times = 2;
            returns(Optional.of(new MetricsStorageUtils.MetricsDef(metricString, "samplehash", "sampleMetricId", true)),
                    Optional.of(new MetricsStorageUtils.MetricsDef(metricString, "samplehash", "sampleMetricId", true)));
        }};

        final String appid = "mocked-app";
        final String metricid = "mocked-metricid";

        new JavaTestKit(system) {{

            final Props props = MetricsDefinitionCacheActor.props();
            final ActorRef subject = system.actorOf(props);

            // can also use JavaTestKit “from the outside”
            final JavaTestKit probe = new JavaTestKit(system);

            // the run() method needs to finish within 3 seconds

            MetricsDefinitionCacheActor.MetricsDefinitionActorMessage msg = new MetricsDefinitionCacheActor.MetricsDefinitionActorMessage(appid, metricid, MetricsDefinitionCacheActor.Action.GET);

            final FiniteDuration duration = duration("40 seconds");

            new Within(duration) {
                protected void run() {
                    subject.tell(msg, getRef());
                    // Will wait for the rest of the 3 seconds
                    MetricsDefinitionCacheActor.MetricsDefinitionActorReplyMessage replyMessage = expectMsgClass(MetricsDefinitionCacheActor.MetricsDefinitionActorReplyMessage.class);

                    assertTrue(replyMessage.application == appid);
                    assertTrue(replyMessage.metricId == metricid);
                }
            };

            //this new message does not cause the retrieval from store.
            new Within(duration) {
                protected void run() {
                    subject.tell(msg, getRef());
                    // Will wait for the rest of the 3 seconds
                    MetricsDefinitionCacheActor.MetricsDefinitionActorReplyMessage replyMessage = expectMsgClass(MetricsDefinitionCacheActor.MetricsDefinitionActorReplyMessage.class);

                    assertTrue(replyMessage.application == appid);
                    assertTrue(replyMessage.metricId == metricid);
                }
            };


            String unknown_appid = appid;//"unknown_appid";
            String unknown_metricid = metricid;//"unknown_metricid";

            //send an invalidate message
            final MetricsDefinitionCacheActor.MetricsDefinitionActorMessage msgInvalidate =
                    MetricsDefinitionCacheActor.MetricsDefinitionActorMessage.createInvalidateMessage(unknown_appid, unknown_metricid);
            subject.tell(msgInvalidate, getRef());

            //lets request it again!!. This should cause the retrieval of the definition from storage again
            final MetricsDefinitionCacheActor.MetricsDefinitionActorMessage msg1 = new MetricsDefinitionCacheActor.MetricsDefinitionActorMessage(unknown_appid, unknown_metricid, MetricsDefinitionCacheActor.Action.GET);

            new Within(duration) {
                protected void run() {
                    subject.tell(msg1, getRef());
                    // Will wait for the rest of the 3 seconds
                    MetricsDefinitionCacheActor.MetricsDefinitionActorReplyMessage replyMessage = expectMsgClass(MetricsDefinitionCacheActor.MetricsDefinitionActorReplyMessage.class);

                    assertTrue(replyMessage.application == unknown_appid);
                    assertTrue(replyMessage.metricId == unknown_metricid);

                    logger.debug("Replied: {}", replyMessage);
                    assertTrue(!replyMessage.isError());

                }
            };

        }};

        new Verifications() {{

        }};
    }

    @Test
    public void testSerializeAndDeserialize() throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        MetricsDefinitionCacheActor.MetricsDefinitionActorMessage getMessage
                = MetricsDefinitionCacheActor.MetricsDefinitionActorMessage.createGetMessage("application", "metricId");

        String serialized = mapper.writeValueAsString(getMessage);
        logger.info("Serialized: {}", serialized);

        MetricsDefinitionCacheActor.MetricsDefinitionActorMessage metricsDefinitionActorMessage =
                mapper.readValue(serialized, MetricsDefinitionCacheActor.MetricsDefinitionActorMessage.class);

        assertTrue(getMessage.equals(metricsDefinitionActorMessage));

        MetricsDefinitionCacheActor.MetricsDefinitionActorMessage invalidateMessage
                = MetricsDefinitionCacheActor.MetricsDefinitionActorMessage.createInvalidateMessage("application", "metricId");

        serialized = mapper.writeValueAsString(invalidateMessage);
        logger.info("Serialized Invalidate: {}", serialized);

        metricsDefinitionActorMessage =
                mapper.readValue(serialized, MetricsDefinitionCacheActor.MetricsDefinitionActorMessage.class);

        assertTrue(invalidateMessage.equals(metricsDefinitionActorMessage));


        MetricsDefinitionCacheActor.MetricsDefinitionActorMessage invalidateAllMessage
                = MetricsDefinitionCacheActor.MetricsDefinitionActorMessage.createInvalidateAllMessage();

        serialized = mapper.writeValueAsString(invalidateAllMessage);
        logger.info("Serialized Invalidate All: {}", serialized);

        metricsDefinitionActorMessage =
                mapper.readValue(serialized, MetricsDefinitionCacheActor.MetricsDefinitionActorMessage.class);

        assertTrue(invalidateAllMessage.equals(metricsDefinitionActorMessage));

    }

    @AfterClass
    public static void tearDown() throws Exception {
        if (system != null) {
            system.terminate();
        }
    }

}