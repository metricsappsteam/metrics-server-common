package es.kibu.geoapis.metrics.engine.core;

import es.kibu.geoapis.objectmodel.Action;
import org.junit.Test;

import static org.junit.Assert.fail;

/**
 * Created by lrodriguez2002cu on 18/01/2017.
 * Some tests related to the actions references.
 */
public class ActionRefTest {

    @Test
    public void testCreation(){

        String target = Action.TARGET_APPLICATION;
        String type =  Action.PUSH_NOTIFICATION;

        ActionRef id = new ActionRef("sampleApp", "sampleUser", "sampleSession",
                "sampleAction", "sampleMetric", type, target);

        try {

            ActionRef id1 = new ActionRef(null, "sampleUser", "sampleSession",
                    "sampleAction", "sampleMetric", type, target);
            fail();
        }
        catch (Error e) {
            System.out.println(e);
        }


        try {
            ActionRef id2 = new ActionRef("application", "sampleUser", "sampleSession",
                    null, "sampleMetric", type, target);
            fail();
        } catch (Error e) {

        }

        try {
            ActionRef id3 = new ActionRef("application", "sampleUser", "sampleSession",
                    "action", null, type, target);
            fail();
        } catch (Error e) {

        }
        try {
            ActionRef id3 = new ActionRef("application", "sampleUser", "sampleSession",
                    "action", "metricId", null, target);
            fail();
        } catch (Error e) {

        }

        try {
            ActionRef id3 = new ActionRef("application", "sampleUser", "sampleSession",
                    "action", "metricId", type, null);
            fail();
        } catch (Error e) {

        }


    }

}