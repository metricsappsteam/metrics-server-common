package es.kibu.geoapis.metrics.engine.data.writing;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by lrodriguez2002cu on 29/03/2017.
 */
public class NamingConventionsTest {

    Logger logger = LoggerFactory.getLogger(NamingConventionsTest.class);

    @Test
    public void applicationNameTest() {
        String appId = NamingConventions.ApplicationIdCreator.createId();
        logger.info("id:" + appId);
        logger.info("length:" + appId.length());
    }

}