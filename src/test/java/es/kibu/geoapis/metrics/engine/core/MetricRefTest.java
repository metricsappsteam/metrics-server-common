package es.kibu.geoapis.metrics.engine.core;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by lrodr_000 on 18/10/2016.
 */
public class MetricRefTest {

    @Test
    public void equals() throws Exception {
        MetricRef metricRef = new MetricRef("app", "metric");
        MetricRef metricRef1 = new MetricRef("app", "metric");
        assertTrue(metricRef.equals(metricRef1));
    }

    @Test
    public void notEquals() throws Exception {
        MetricRef metricRef = new MetricRef("app", "metric");
        MetricRef metricRef1 = new MetricRef("app1", "metric");
        assertFalse(metricRef.equals(metricRef1));


        metricRef = new MetricRef("app1", "metric1");
        metricRef1 = new MetricRef("app1", "metric");
        assertFalse(metricRef.equals(metricRef1));

    }

}