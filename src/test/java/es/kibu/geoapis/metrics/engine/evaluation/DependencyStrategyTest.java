package es.kibu.geoapis.metrics.engine.evaluation;

/*
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import es.kibu.geoapis.metrics.engine.MetricsBackendUtils;
import es.kibu.geoapis.metrics.engine.core.DefaultStatefulMetricsEngine;
import es.kibu.geoapis.metrics.engine.core.StatefulMetricsEngine;
*/
import es.kibu.geoapis.objectmodel.MetricsDefinition;
import mockit.Expectations;
import mockit.Mocked;
import mockit.integration.junit4.JMockit;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * Created by lrodr_000 on 10/09/2016.
 */
@RunWith(JMockit.class)
public class DependencyStrategyTest {


    Logger logger = LoggerFactory.getLogger(DependencyStrategy.class);

    @Test
    public void testASimpleChain(@Mocked MetricsDefinition definition) {
        String sampleScript =
                " var a = 0; " +
                        "var metric2 = function() { return metric3(1); } ; " +
                        "function metric3() { return metric4(1); }  " +
                        "var metric4 = function () { return a; };";


        //to mock so that only the ones shown are send
        DefaultJSDependencyAnalyzer analyzer = new DefaultJSDependencyAnalyzer(sampleScript);
        analyzer.analyze();

        List<String> varsAndFucntions = new ArrayList<>();
        varsAndFucntions.add("a");
        varsAndFucntions.add("metric2");
        varsAndFucntions.add("metric3");
        varsAndFucntions.add("metric4");

        DependencyStrategy strategy = new DependencyStrategy(definition, analyzer);
        ;
        //expectations
        new Expectations(strategy) {{
            strategy.getAllNames(definition);
            result = varsAndFucntions;
        }};

        //record
        strategy.init();
        List<String> callOrderList = strategy.calculateStrategy("a", DependencyStrategy.StrategyType.ONLY_DIRECT_CALLERS);
        logger.debug(callOrderList.toString());

        //verifications
        assertTrue(callOrderList.size() == 1);
        assertTrue(callOrderList.get(0).equalsIgnoreCase("metric4"));

        List<String> callOrderAllWhoUses = strategy.calculateStrategy("a", DependencyStrategy.StrategyType.ALL_WHO_USES);
        logger.debug(callOrderAllWhoUses.toString());
        assertTrue(callOrderAllWhoUses.size() == 3);
        //first is called metric4 as the first (direct) caller of the chain
        assertTrue(callOrderAllWhoUses.get(0).equalsIgnoreCase("metric4"));

        //then comes metric3 as the one that deppends on metric4
        assertTrue(callOrderAllWhoUses.get(1).equalsIgnoreCase("metric3"));

        //finally comes metric2
        assertTrue(callOrderAllWhoUses.get(2).equalsIgnoreCase("metric2"));

    }

    @Test
    public void testABitMoreComplexWithOrderDrivedByDependencies(@Mocked MetricsDefinition definition) {
        /*This script would show that of only variables b is updated, 
        then only dependent functions (i.e metrics would)*/
        String sampleScript =
                " var a = 0; " +
                        " var b = 1; " +
                        " var c = 1; " +
                        " var metric2 = function() { return metric4() + c; } ; " +
                        " function metric3() { return metric2(1); }  " +
                        " var metric4 = function () { return a + c; };";

        //to mock so that only the ones shown are send
        DefaultJSDependencyAnalyzer analyzer = new DefaultJSDependencyAnalyzer(sampleScript);
        analyzer.analyze();

        List<String> varsAndFucntions = new ArrayList<>();
        //this is a way of telling that all must be analyzed
        varsAndFucntions.add("a");
        varsAndFucntions.add("b");
        varsAndFucntions.add("c");
        varsAndFucntions.add("metric2");
        varsAndFucntions.add("metric3");
        varsAndFucntions.add("metric4");

        DependencyStrategy strategy = new DependencyStrategy(definition, analyzer);
        ;
        //expectations
        new Expectations(strategy) {{
            strategy.getAllNames(definition);
            result = varsAndFucntions;
        }};

        //record
        strategy.init();
        List<String> callOrderList = strategy.calculateStrategy("c", DependencyStrategy.StrategyType.ONLY_DIRECT_CALLERS);
        logger.debug(callOrderList.toString());

        //verifications
        assertTrue(callOrderList.size() == 2);

        //both metric 2 and metric 4 must be called.. but which  first?
        //As metric2 deppends on metric4 must be called first
        assertTrue(callOrderList.get(0).equalsIgnoreCase("metric4"));
        assertTrue(callOrderList.get(1).equalsIgnoreCase("metric2"));


    }


    @Test
    public void testAMoreComplex(@Mocked MetricsDefinition definition) {
        /*This script would show that of only variables b is updated,
        then only dependent functions (i.e metrics would)*/
        String sampleScript =
                " var b = 0; " +
                        " var a = b; " +
                        "var metric2 = function() { return b; } ; " +
                        "function metric3() { return metric2(1); }  " +
                        "var metric4 = function () { return a; };";


        //to mock so that only the ones shown are send
        DefaultJSDependencyAnalyzer analyzer = new DefaultJSDependencyAnalyzer(sampleScript);
        analyzer.analyze();

        List<String> varsAndFucntions = new ArrayList<>();
        varsAndFucntions.add("a");
        varsAndFucntions.add("b");
        varsAndFucntions.add("metric2");
        varsAndFucntions.add("metric3");
        varsAndFucntions.add("metric4");

        DependencyStrategy strategy = new DependencyStrategy(definition, analyzer);
        ;
        //expectations
        new Expectations(strategy) {{
            strategy.getAllNames(definition);
            result = varsAndFucntions;
        }};

        //record
        strategy.init();
        List<String> callOrderList = strategy.calculateStrategy("a", DependencyStrategy.StrategyType.ONLY_DIRECT_CALLERS);
        logger.debug(callOrderList.toString());

        //verifications
        assertTrue(callOrderList.size() == 1);
        assertTrue(callOrderList.get(0).equalsIgnoreCase("metric4"));

        List<String> callersB = strategy.calculateStrategy("b", DependencyStrategy.StrategyType.ONLY_DIRECT_CALLERS);
        logger.debug(callersB.toString());

        //verifications
        assertTrue(callersB.size() == 1);
        assertTrue(callersB.get(0).equalsIgnoreCase("metric2"));


        List<String> callOrderAllWhoUsesA = strategy.calculateStrategy("a", DependencyStrategy.StrategyType.ALL_WHO_USES);
        logger.debug(callOrderAllWhoUsesA.toString());
        assertTrue(callOrderAllWhoUsesA.size() == 1);
        //first is called metric4 as the first (direct) caller of the chain
        assertTrue(callOrderAllWhoUsesA.get(0).equalsIgnoreCase("metric4"));


        List<String> callOrderAllWhoUsesB = strategy.calculateStrategy("b", DependencyStrategy.StrategyType.ALL_WHO_USES);
        logger.debug(callOrderAllWhoUsesB.toString());
        assertTrue(callOrderAllWhoUsesB.size() == 3);

        //first is called metric2 as the first (direct) caller of the chain
        assertTrue(callOrderAllWhoUsesB.get(0).equalsIgnoreCase("metric2"));

        //finally comes metric3 who calls metric2
        assertTrue(callOrderAllWhoUsesB.get(1).equalsIgnoreCase("metric3"));

        assertTrue(callOrderAllWhoUsesB.get(2).equalsIgnoreCase("metric4"));

    }


    @Test
    public void testMultipleVariables(@Mocked MetricsDefinition definition) {
        /*This script would show that of only variables b is updated,
        then only dependent functions (i.e metrics would)*/
        String sampleScript =
                " var b = 0; " +
                        " var a = b; " +
                        "var metric2 = function() { return b; } ; " +
                        "function metric3() { return metric2(1); }  " +
                        "var metric4 = function () { return a; };";


        //to mock so that only the ones shown are send
        DefaultJSDependencyAnalyzer analyzer = new DefaultJSDependencyAnalyzer(sampleScript);
        analyzer.analyze();

        List<String> varsAndFucntions = new ArrayList<>();
        varsAndFucntions.add("a");
        varsAndFucntions.add("b");
        varsAndFucntions.add("metric2");
        varsAndFucntions.add("metric3");
        varsAndFucntions.add("metric4");

        DependencyStrategy strategy = new DependencyStrategy(definition, analyzer);
        //expectations
        new Expectations(strategy) {{
            strategy.getAllNames(definition);
            result = varsAndFucntions;
        }};

        //record
        strategy.init();

        List<String> variables = new ArrayList<>();
        variables.add("a");
        variables.add("b");

        List<String> callOrderList = strategy.calculateStrategy(variables, DependencyStrategy.StrategyType.ONLY_DIRECT_CALLERS);
        logger.debug(callOrderList.toString());

        //verifications
        assertTrue(callOrderList.size() == 2);
        assertTrue(callOrderList.contains("metric4"));
        assertTrue(callOrderList.contains("metric2"));

        List<String> callersB = strategy.calculateStrategy(variables, DependencyStrategy.StrategyType.ALL_WHO_USES);
        logger.debug(callersB.toString());

        //verifications
        assertTrue(callersB.size() == 3);
        assertTrue(callersB.contains("metric2"));
        assertTrue(callersB.contains("metric4"));
        assertTrue(callersB.get(2).equalsIgnoreCase("metric3"));

    }

    @Test
    public void testMultipleVariablesWithMoreComplexOrder(@Mocked MetricsDefinition definition) {
        /*This script would show that of only variables b is updated,
        then only dependent functions (i.e metrics would)*/
        String sampleScript =
                " var a = 0; " +
                        " var a = b; " +
                        "var metric2 = function() { return b; } ; " +
                        "function metric3() { return metric2(1); }  " +
                        "var metric4 = function () { return a + metric3(); };";


        //to mock so that only the ones shown are send
        DefaultJSDependencyAnalyzer analyzer = new DefaultJSDependencyAnalyzer(sampleScript);
        analyzer.analyze();

        List<String> varsAndFucntions = new ArrayList<>();
        varsAndFucntions.add("a");
        varsAndFucntions.add("b");
        varsAndFucntions.add("metric2");
        varsAndFucntions.add("metric3");
        varsAndFucntions.add("metric4");

        DependencyStrategy strategy = new DependencyStrategy(definition, analyzer);
        //expectations
        new Expectations(strategy) {{
            strategy.getAllNames(definition);
            result = varsAndFucntions;
        }};

        //record
        strategy.init();

        List<String> variables = new ArrayList<>();
        variables.add("a");
        variables.add("b");

        List<String> callOrderList = strategy.calculateStrategy(variables, DependencyStrategy.StrategyType.ONLY_DIRECT_CALLERS);
        logger.debug(callOrderList.toString());

        //verifications
        assertTrue(callOrderList.size() == 2);
        assertTrue(callOrderList.contains("metric4"));
        assertTrue(callOrderList.contains("metric2"));

        List<String> callersB = strategy.calculateStrategy(variables, DependencyStrategy.StrategyType.ALL_WHO_USES);
        logger.debug(callersB.toString());

        //verifications
        assertTrue(callersB.size() == 3);
        assertTrue(callersB.get(0).equalsIgnoreCase("metric2"));
        assertTrue(callersB.get(1).equalsIgnoreCase("metric3"));
        assertTrue(callersB.get(2).equalsIgnoreCase("metric4"));


    }

    //TODO: Move this to engine tests!!
  /*  @Test
    public void testMultipleVariablesRealScenario() throws IOException, ProcessingException
    {

        MetricsDefinition definition = MetricsBackendUtils.withMetricDefinition("sample-metrics/sample-metric-simple-spark.json");
        StatefulMetricsEngine engine = new DefaultStatefulMetricsEngine(true);

        try {
            engine.init(definition);
            String script = engine.getScript();

            logger.debug(script);
            assertTrue(!script.isEmpty());

            //to mock so that only the ones shown are send
            DefaultJSDependencyAnalyzer analyzer = new DefaultJSDependencyAnalyzer(script);
            analyzer.analyze();

          *//*  List<String> varsAndFucntions = new ArrayList<>();
            varsAndFucntions.add("movement");
            varsAndFucntions.add("simpleMetric");
            varsAndFucntions.add("simpleMetricScopes");*//*

            DependencyStrategy strategy = new DependencyStrategy(definition, analyzer);

            //record
            strategy.init();

            List<String> variables = new ArrayList<>();
            variables.add("movement");
            variables.add("simpleMetric");
            variables.add("simpleMetricScopes");

            //strategy.getNextMetric();

            List<String> callOrderList = strategy.calculateStrategy(variables, DependencyStrategy.StrategyType.ALL_WHO_USES);
            logger.debug(callOrderList.toString());

       *//*     //verifications
            assertTrue(callOrderList.size() == 2);
            assertTrue(callOrderList.contains("metric4"));
            assertTrue(callOrderList.contains("metric2"));

            List<String> callersB = strategy.calculateStrategy(variables, DependencyStrategy.StrategyType.ALL_WHO_USES);
            logger.debug(callersB);

            //verifications
            assertTrue(callersB.size() == 3);
            assertTrue(callersB.get(0).equalsIgnoreCase("metric2"));
            assertTrue(callersB.get(1).equalsIgnoreCase("metric3"));
            assertTrue(callersB.get(2).equalsIgnoreCase("metric4"));
*//*
        } finally {
            engine.stop();
        }

    }*/


}