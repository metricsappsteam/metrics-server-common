package es.kibu.geoapis.services;

import akka.actor.ActorSystem;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import com.typesafe.config.ConfigValue;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * Created by lrodriguez2002cu on 12/03/2017.
 */

public class MetricsManagerTest {

    static ActorSystem system;
    static Logger logger = LoggerFactory.getLogger(MetricsManagerTest.class);

    @BeforeClass
    public static void setUp(){

        //System.setProperty("actors.db.uri", "localhost:27017");
        Config config =  ConfigFactory.parseString("actors.db.uri=\"localhost:27017\" \n actors.db.name=\"metrics-database\" "  )
                //.parseString("actors.db.uri=\"localhost:27017\"")
                .withFallback(ConfigFactory.load());
        //Config config = ConfigFactory.load().withValue("actors.db.uri", "localhost:27017")
        system = ActorSystem.create("MetricsManagerTest", config);

        for (Map.Entry<String, ConfigValue> entry: system.settings().config().entrySet()) {
            logger.info("key: {}, value:{}", entry.getKey(), entry.getValue());
        }
    }

    @Test
    @Ignore
    public void testGetMetricsDef() throws Exception {
        MetricsManager manager = MetricsStorageUtils.getMetricsManager(system);
        String nonExistingApp = "unknownApp";
        //nonExistingApp = "sample-appid1482872523312";
        nonExistingApp = "application-46169671277633484794616967127763348479";

        for (int i = 0; i < 10000; i++) {
            if (manager.getMetricsDef(nonExistingApp).isPresent()) {
                logger.debug("ApplicationMetricsFound app: {}", nonExistingApp);
            }
        }
    }

    public void testUpdateMetricsDef() throws Exception {

    }

    public void testDeleteMetricsDef() throws Exception {

    }

    public void testCreateMetricsDef() throws Exception {

    }

    public void testMetricsExistsForApp() throws Exception {

    }

}